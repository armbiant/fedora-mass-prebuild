#!/bin/bash
Red='\033[0;31m'          # Red
Green='\033[0;32m'        # Green
Blue='\033[0;34m'         # Blue
Color_Off='\033[0m'       # Text Reset

DISTRIB='fedora'
CODENAME='latest'
REVISION='1'
SCRIPTS=''

usage="Usage: $(basename ${0}) -d{distribution} -r{revision}"
where=" Where :\n
    D: Distribution (default: \"${DISTRIB}\")\n
    R: Release codename (default: \"${CODENAME}\")\n
    r: Revision to tag (e.g. 6, default: \"${REVISION}\")\n
    d: Enter the container at the end of the process for debugging\n
    s: Regression scripts to execute instead of all (mpb install, regression\n
       header and footer are automatically added) e.g. $(basename ${0}) -s '007* 008*'\n
"
OPTS=`getopt -o 'dD:r:R:s:' -- "$@"`

ret=$?

if [ ! ${ret} = 0 ]
then
  echo ${usage};
  echo -e ${where};
  exit 1
fi

eval set -- "$OPTS"

while true;
do
  case "$1" in
    -D ) DISTRIB="$2"; shift 2;;
    -R ) CODENAME="$2"; shift 2;;
    -r ) REVISION="$2"; shift 2;;
    -d ) DEBUG="debug"; shift;;
    -s ) SCRIPTS="$2"; shift 2;;
    --) shift; break;;
    * )
        echo $usage;
        echo -e $where;
        exit 1;;
  esac
done

if ! command -v podman &> /dev/null
then
    echo "podman is needed for this script to be used"
    exit 2
fi

root=$(basename $(git rev-parse --show-toplevel))
echo $root

$(dirname ${0})/build_container.sh -D ${DISTRIB} -R ${CODENAME} -r ${REVISION}

CONTAINER_ID=$(podman run -d -i --net host ${DISTRIB}:${CODENAME}.${REVISION} /bin/bash)

cd $(dirname ${0})

echo -e "${Blue}Copying the repository into the container${Color_Off}"
podman exec ${CONTAINER_ID} rm -rf /mpb/${root}
podman cp $(git rev-parse --show-toplevel)/ ${CONTAINER_ID}:/mpb/

podman exec ${CONTAINER_ID} mkdir -p /home/mpb/.config
podman cp ${HOME}/.config/copr ${CONTAINER_ID}:/home/mpb/.config

timeout 2h podman exec ${CONTAINER_ID} /mpb/${root}/tests/ci/all.sh ${SCRIPTS}

ret=$?

if [ "x${DEBUG}" == "x" ] && [ ! "$ret" == "124" ]
then
  podman container stop -t 1 ${CONTAINER_ID} > /dev/null 2>&1
  podman container rm -f ${CONTAINER_ID} > /dev/null 2>&1
else
  podman exec -it ${CONTAINER_ID} /bin/bash

  echo -e "${Blue}You can destroy the container using the following command:${Color_Off}"
  echo "podman container rm -f -t 2 ${CONTAINER_ID}"
fi

exit ${ret}
