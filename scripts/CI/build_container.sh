#!/bin/bash

DISTRIB='fedora'
CODENAME='latest'
REVISION='1'

usage="Usage: $(basename ${0}) -d{distribution} -r{revision}"
where=" Where :\n
	D: Distribution (default: \"${DISTRIB}\")\n
	R: Release codename (default: \"${CODENAME}\")\n
	r: Revision to tag (e.g. 6, default: \"${REVISION}\")\n
"
OPTS=`getopt -o 'D:r:R:' -- "$@"`

ret=$?

if [ ! ${ret} = 0 ]
then
  echo ${usage};
  echo -e ${where};
  exit 1
fi

eval set -- "$OPTS"

while true;
do
  case "$1" in
    -D ) DISTRIB="$2"; shift 2;;
    -R ) CODENAME="$2"; shift 2;;
    -r ) REVISION="$2"; shift 2;;
    --) shift; break;;
    * )
        echo $usage;
        echo -e $where;
        exit 1;;
  esac
done

if ! command -v podman &> /dev/null
then
    echo "podman is needed for this script to be used"
    exit 2
fi

echo "podman build -t ${DISTRIB}:${CODENAME}.${REVISION} --squash --no-cache --force-rm \
  --build-arg user=mpb --build-arg uid=$UID \
  --build-arg DISTRIB=${DISTRIB} \
  --build-arg CODENAME=${CODENAME} \
  --network host $(dirname ${0})"

podman image rm ${DISTRIB}:${CODENAME}.${REVISION}
podman build -t ${DISTRIB}:${CODENAME}.${REVISION} --squash --no-cache --force-rm \
  --build-arg user=mpb --build-arg uid=$UID \
  --build-arg DISTRIB=${DISTRIB} \
  --build-arg CODENAME=${CODENAME} \
  --network host $(dirname ${0})
