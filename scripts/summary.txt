This release brings new tools for the user:

 - mpb-failedconf script allows to generate a configuration file which
   contains the package that have failed in a given build, which allows
   to have a short-cut to verify a fix.
 - mpb-copr-edit script allows the user to edit a copr project in order
   to massively enable the webhook-rebuild option. This can only be used
   if the original MPB build is done.

It is now possible to provide a full configuration to COPR project,
which may contain a dedicated description, specific YUM/DNF repositories
or even a different mock configuration.

And, as always, various improvement and bug fixes, such as:

 - Once and MPB build has been cleaned, the user may execute the 'mpb
   --cleanup' command which will remove the corresponding entries from the
   database.
 - Support for verbosity in the --list command, which may now show the
   builds that have been "cleaned" and the control builds
 - COPR connection cache is automatically cleared in case of connection
   failure
