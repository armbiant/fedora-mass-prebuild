""" Mass pre-builder main interface

    The mass pre-builder is a set of tools meant to help the user to build its
    package and any reverse dependencies from it. The overall goal is to assess
    if a change in the main package obviously affects negatively its reverse
    dependencies. The infrastructure will not only build the packages but also
    execute the associated tests. If any error is found during the process, the
    user is informed from this failure.
"""

import argparse
import logging
from pathlib import Path
import re
import sys
import threading
import yaml

from . import VERSION

from .utils.whatrequires import load_default_repo_conf, repos_from_file
from .backend import copr, dummy
from .backend.db import authorized_archs, authorized_collect, authorized_status_base, MpbDb
from .backend.package import MAIN_PKG, NO_BUILD, ALL_DEP_PKG


def handle_thread_exception(args):
    """Redirect thread exception to the general exceptions"""
    if args.exc_type == SystemExit:
        return
    sys.excepthook(args.exc_type, args.exc_value, args.exc_traceback)


def handle_exception(exc_type, exc_value, exc_traceback):
    """Log exception into a file to ease debugging"""
    if exc_type == SystemExit:
        return

    if issubclass(exc_type, KeyboardInterrupt):
        sys.__excepthook__(exc_type, exc_value, exc_traceback)
        return

    logger().error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    if current_backend() is not None:
        current_backend().stop_agents(False)

    release_database()

    print('Stopping due to Uncaught exception !')
    print(f'Exception log stored in {str(Path(Path.home() / ".mpb" / "mpb.log"))}')


def database(config=None):
    """Setup and return the database"""
    if not hasattr(database, "database"):
        database.database = None

    if config:
        database.database = MpbDb(path=config['database'], verbose=config['verbose'])

    return database.database


def logger():
    """Setup and return the logger"""
    if not hasattr(logger, "logger"):
        home_path = Path(Path.home() / '.mpb')
        home_path.mkdir(parents=True, exist_ok=True)

        logging.basicConfig(
            filename=str(Path(home_path / 'mpb.log')),
            filemode='a',
            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
            datefmt='%H:%M:%S',
            level=logging.ERROR,
        )

        logger.logger = logging.getLogger(__name__)

    return logger.logger


def backends():
    """List of available back-ends"""
    return {'dummy': dummy.DummyBackend, 'copr': copr.MpbCoprBackend}


def current_backend(config=None):
    """Global backend accessors"""
    if not hasattr(current_backend, "backend"):
        current_backend.backend = None

    if config:
        current_backend.backend = backends()[config['backend']](database(), config)

    return current_backend.backend


def load_repos(config):
    """Load dnf repositories information"""
    verbose = config['verbose']
    dnf_conf = config['dnf_conf']

    if config['build_id']:
        build = database().build_by_id(config['build_id'])
        dnf_conf = build['dnf_conf']

    try:
        load_default_repo_conf(verbose)
    except FileNotFoundError as exc:
        print(exc)
        release_database()
        sys.exit(1)

    try:
        repos_from_file(dnf_conf, verbose)
    except ValueError as exc:
        print(exc)
        release_database()
        sys.exit(1)


def print_config(config):
    """Print the current configuration if needed"""
    if not all([config['verbose'] > 2, not config['info']]):
        return

    print('Using the following config: ')
    print(f'{yaml.dump(config, default_flow_style=False)}')


def clean_builds(config):
    """Remove cleaned builds from the database"""
    if not config['cleanup']:
        return

    builds = database().builds()

    builds = [b for b in builds if b['state'] == 'FREE']

    for build in builds:
        database().remove(build['build_id'])


def list_builds(config):
    """Print a comprehensive list of available builds"""
    if not config['list']:
        return

    verbose = config['verbose']

    builds = database().builds()
    max_len = 14

    for build in builds:
        if all([verbose < 1, build['name'].endswith('.checker')]):
            continue
        if all([verbose < 2, build['state'] == 'FREE']):
            continue
        max_len = max(max_len, len(build['name']))

    build_id = 'Build ID'.center(16)
    name = 'Name'.center(max_len)
    backend = 'Backend'.center(16)
    state = 'Last state'.center(16)
    stage = 'Last stage'.center(16)

    print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')
    print(f'|{build_id}| {name} |{backend}|{state}|{stage}|')
    print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')

    for build in builds:
        if all([verbose < 1, build['name'].endswith('.checker')]):
            continue
        if all([verbose < 2, build['state'] == 'FREE']):
            continue

        build_id = str(build['build_id']).center(16)
        name = build['name'].ljust(max_len)
        config = yaml.safe_load(build['config'])
        backend = 'unknown'.center(16)

        if 'backend' in config:
            backend = config['backend'].center(16)

        state = build['state'].center(16)
        stage = str(build['stage']).center(16)

        print(f'|{build_id}| {name} |{backend}|{state}|{stage}|')

    print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')


def list_packages(config):
    """List packages if requested"""
    if config['list_packages']:
        if config['verbose'] > 4:
            print(f'Package list for {config["build_id"]} ({current_backend().name})')
        current_backend().print_packages()


def collect(config):
    """Collect data from build if requested"""
    if config['collect']:
        print(f'Collecting data for {current_backend().name}')
        current_backend().collect_data()


def cancel(config):
    """Cancel packages build if requested"""
    if config['cancel']:
        print(f'Canceling {config["build_id"]} ({current_backend().name})')
        current_backend().cancel()


def clean(config):
    """Clean build if requested"""
    if config['clean']:
        print(f'Cleaning build {config["build_id"]} ({current_backend().name})')
        current_backend().clean()


def infos(config):
    """Print build info if needed"""
    if config['info']:
        if config['verbose'] > 4:
            print(f'Printing info about {config["build_id"]} ({current_backend().name})')
        current_backend().info()

def rebuild(config):
    """Rebuild packages if required"""

    if not config['rebuild']:
        return

    print('Preparing to rebuild packages.')

    if isinstance(config['rebuild'], str):
        config['rebuild'] = config['rebuild'].split()

    if config['rebuild'][0].lower() == 'failed':
        status = [ 'FAILED', 'UNCONFIRMED', 'CHECK_NEEDED' ]
    else:
        status = authorized_status_base | { 'PENDING' }

    for pkg in current_backend().packages:
        if pkg.pkg_type & (MAIN_PKG | NO_BUILD):
            continue

        if not any([pkg.name in config['rebuild'],
                pkg.src_pkg_name in config['rebuild'],
                config['rebuild'][0].lower() in ['failed', 'all']]):
            continue

        # Ensure status is up to date
        pkg.check_status(pkg.pkg_type & ALL_DEP_PKG)
        if pkg.build_status not in status:
            continue

        current_backend().rebuild(pkg)

    old_conf = current_backend().config
    old_conf['stage'] = 3

    print('Reloading back-end to initiate rebuild.')
    current_backend(old_conf)

def _get_config(config_file):
    """Load a config file provided by the user"""
    try:
        with open(config_file, encoding='utf-8') as file:
            conf = {}
            try:
                conf = dict(yaml.safe_load(file))
            except yaml.YAMLError as exc:
                print(exc)
            return conf
    except FileNotFoundError:
        if config_file is not None:
            print(f'Can\'t open config file {config_file}')

    return {}


def get_config(config_file):
    """Load a config file provided by the user"""
    path = Path(config_file)
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config(config_file)

    path = Path("mpb.config")
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config("mpb.config")

    home_path = Path(Path.home() / '.mpb')
    home_path.mkdir(parents=True, exist_ok=True)

    path = Path(home_path / 'config')
    if path.is_file():
        print(f'Loading {str(path)}')
        return _get_config(str(Path(home_path / 'config')))

    return {}


def is_package_config_valid(config, ptype):
    """Make basic check on the configuration file"""
    ret = True

    if isinstance(config[ptype], str):
        config[ptype] = config[ptype].split()

    if isinstance(config[ptype], list):
        config[ptype] = {p: {'src_type': 'distgit'} for p in config[ptype]}

    if not isinstance(config[ptype], dict):
        print(
            f'Provided config file is malformed (packages is no dictionary: {type(config[ptype])})'
        )
        return False

    for name, pkg in config[ptype].items():
        if not isinstance(pkg, dict):
            # Early adopters need to update their config[ptype] files
            print(f'Config for {name} is malformed')
            ret = False
            continue

        if any(
            [
                not re.match(r'^@?[a-zA-Z0-9-._+]+', name),
                # Reject (s)rpm file names
                re.search(r'\.s?rpm$', name),
                re.search(r'\.src\.rpm$', name),
                # Reject NVR names
                re.search(r'\.[a-z]{1,3}[0-9]{1,4}$', name),
            ]
        ):
            print(f'Package name "{name}" is invalid')
            print('Don\'t provide source rpm, rpm or NEVRA names')
            ret = False

        pkg.setdefault('src_type', 'distgit')
        pkg.setdefault('skip_archs', '')
        pkg.setdefault('deps_only', False)

        src_type = pkg['src_type']
        if src_type in ['file', 'script']:
            if 'src' not in pkg:
                print(f'Config is missing source for {name}:{src_type}')
                ret = False
                break

            path = Path(pkg['src'])
            if not path.is_file():
                print(
                    f'Path provided for {pkg}:{src_type} doesn\'t exists (got {str(path)})'
                )
                ret = False
                break

        if 'retry' in pkg:
            if pkg['retry'] == 'dynamic':
                pkg['retry'] = -1

            try:
                pkg['retry'] = int(pkg['retry'])
            except ValueError:
                print(f'Retry count for {name} in invalid: {pkg["retry"]}')
                ret = False

        validate_skip_archs(pkg)

    return ret


def validate_packages(config):
    """Validate packages config"""
    # Check all elements separately so that user can see all errors at once
    if config['verbose'] > 4:
        print('Checking for package configuration validity.')

    if 'packages' in config:
        config['validity'] = is_package_config_valid(config, 'packages')
    else:
        config['packages'] = {}

    if 'revdeps' not in config:
        config['revdeps'] = {}

    for ptype in ['list', 'append']:
        if not config['validity']:
            break

        if ptype not in config['revdeps']:
            continue

        config['validity'] = is_package_config_valid(config['revdeps'], ptype)

    if all([not config['packages'], config['build_id'] <= 0]):
        print('No package list nor build ID given')
        config['validity'] = False


def validate_skip_archs(config):
    """Check if given archs are valid"""
    if 'skip_arch' in config:
        config['skip_archs'] = config['skip_arch']
        del config['skip_arch']

    if isinstance(config['skip_archs'], str):
        config['skip_archs'] = config['skip_archs'].split()

    if isinstance(config['skip_archs'], list):
        config['skip_archs'] = set(config['skip_archs'])

    if isinstance(config['skip_archs'], dict):
        config['skip_archs'] = set(config['skip_archs'].keys())

    for skip_arch in config['skip_archs']:
        if skip_arch not in authorized_archs:
            print(f'{skip_arch} is not a supported skip_architecture')
            config['validity'] = False

    if 'all' in config['skip_archs']:
        print('Skipping all architectures')
        config['validity'] = False


def validate_archs(config):
    """Check if given archs are valid"""
    if 'arch' in config:
        config['archs'] = config['arch']
        del config['arch']

    if isinstance(config['archs'], str):
        config['archs'] = config['archs'].split()

    if isinstance(config['archs'], list):
        config['archs'] = set(config['archs'])

    if isinstance(config['archs'], dict):
        config['archs'] = set(config['archs'].keys())

    for arch in config['archs']:
        if arch not in authorized_archs:
            print(f'{arch} is not a supported architecture')
            config['validity'] = False


def validate_collect(config):
    """Validate collect config"""
    authorized_collect_ctrl = {
        f'control-{col}'.upper() for col in authorized_collect
    }
    accepted = sorted(authorized_collect) + sorted(authorized_collect_ctrl)
    accepted = [p.lower() for p in accepted]

    if isinstance(config['collect_list'], str):
        if ',' in config['collect_list']:
            config['collect_list'] = [
                col.strip() for col in config['collect_list'].split(',')
            ]
        else:
            config['collect_list'] = config['collect_list'].split()

    for col in config['collect_list']:
        if col.lower() not in accepted:
            print(f'Invalid collect option "{col}"')
            print(f'Accepted values: {accepted}')
            config['validity'] = False

    collect_set = set(config['collect_list'])

    for ctrl in ['', 'control-']:
        subcheck = {f'{ctrl}{col.lower()}' for col in authorized_collect}
        subset = collect_set & subcheck
        if all([len(subset - {f'{ctrl}log'}) == 0, f'{ctrl}log' in subset]):
            collect_set |= {f'{ctrl}failed'}

    for ctrl in ['', 'control-']:
        if f'{ctrl}failed' in collect_set:
            collect_set |= {f'{ctrl}check_needed', f'{ctrl}unconfirmed'}

    config['collect_list'] = list(collect_set)

    if all([config['collect'], config['build_id'] <= 0]):
        print('Build ID missing for collect operation')
        config['validity'] = False


def release_database():
    """Release the database locks"""
    if not hasattr(release_database, 'lock'):
        release_database.lock = threading.Lock()

    if not hasattr(release_database, 'released'):
        release_database.released = False

    with release_database.lock:
        if all([database() is not None, not release_database.released]):
            database().release()
            release_database.released = True


def validate_database(config):
    """Validate the database"""
    if not database(config):
        config['validity'] = False


def validate_build_id(config):
    """Validate build ID"""
    if not config['build_id']:
        return

    if not database():
        print('No valid database to check for build ID')
        config['build_id'] = 0
        return

    build = database().build_by_id(config['build_id'])

    if not build:
        print(f'Build ID doesn\'t match any known ID {config["build_id"]}')
        config['build_id'] = 0


def validate_backend(user_config):
    """Validate the provided backend"""
    if any([not database(), user_config['build_id'] <= 0]):
        return

    # First try to get the backend from the database
    build = database().build_by_id(user_config['build_id'])
    build_config = yaml.safe_load(build['config'])

    backend = 'unknown'

    if 'backend' in build_config:
        backend = build_config['backend']

    if backend == 'unknown':
        backend = user_config['backend']
    else:
        user_config['backend'] = backend

    if backend not in ['copr', 'dummy']:
        print(f'Unknown back-end {backend}')
        user_config['validity'] = False


def validate_build_cmds(config):
    """Validate on shots commands"""
    validate_database(config)
    validate_build_id(config)
    validate_backend(config)

    if all([config['cancel'], config['build_id'] <= 0]):
        print('Build ID missing for cancel operation')
        config['validity'] = False
        return
    if all([config['clean'], config['build_id'] <= 0]):
        print('Build ID missing for clean operation')
        config['validity'] = False
        return
    if all([config['info'], config['build_id'] <= 0]):
        print('Build ID missing for info operation')
        config['validity'] = False
        return
    if all([config['list_packages'], config['build_id'] <= 0]):
        print('Build ID missing to list packages')
        config['validity'] = False
        return


def validate_config(config):
    """Validate configuration inputs"""
    validate_build_cmds(config)

    if any(
            [
                config['clean'],
                config['cancel'],
                config['list'],
                config['info'],
                config['list_packages'],
                not config['validity'],
                config['cleanup'],
            ]
        ):
        return

    validate_packages(config)
    if config['retry'] == 'dynamic':
        config['retry'] = -1

    try:
        config['retry'] = int(config['retry'])
    except ValueError:
        print(f'Retry count in invalid: {config["retry"]}')
        config['validity'] = False

    if all([config['stage'] >= 0, config['build_id'] <= 0]):
        print('Stage can\'t be given without a valid build ID.')
        config['validity'] = False

    if config['backend'] not in backends():
        config['validity'] = False
        print(f'Unknown back-end: {config["backend"]}')

    validate_archs(config)
    validate_skip_archs(config)

    # Ensure we have a boolean and not a random string
    config['enable_priorities'] = bool(config['enable_priorities'])

    if config['dnf_conf']:
        if not Path(config['dnf_conf']).is_file():
            print(f'Can\'t find DNF config: {config["dnf_conf"]}')
            config['validity'] = False
        else:
            config['dnf_conf'] = str(Path(config['dnf_conf']).resolve())

    validate_collect(config)


def parse_args():
    """Setup argument parser"""
    authorized_collect_ctrl = {
        f'control-{col}'.upper() for col in authorized_collect
    }
    accepted = sorted(authorized_collect) + sorted(authorized_collect_ctrl)
    accepted = [p.lower() for p in accepted]

    parser = argparse.ArgumentParser()

    parser.add_argument(
        '--verbose',
        '-V',
        action='count',
        help='increase output verbosity, may be provided multiple times',
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument(
        '--config',
        '-c',
        default='',
        help='''provide user specific config
                values in command line supersedes values in config file''',
    )
    parser.add_argument(
        '--stage',
        '-s',
        type=int,
        help='stage to directly begin with (needs a valid build ID)',
    )
    parser.add_argument('--buildid', '-b', type=int, help='build ID to work on')
    parser.add_argument(
        '--cancel',
        action='store_true',
        help='cancel any running package build from the given mass rebuild',
    )
    parser.add_argument(
        '--clean',
        action='store_true',
        help='cancel build and delete a given mass rebuild',
    )
    parser.add_argument(
        '--backend', help='choose a different backend from copr default'
    )

    parser.add_argument(
        '--collect',
        action='store_true',
        help='collect data for the given build id',
    )

    parser.add_argument(
        '--collect-list',
        nargs='?',
        const='',
        help=f'which data to collect, when collect stage is executed (accepted:{accepted})',
    )

    parser.add_argument(
        '--list',
        action='store_true',
        help='list active builds started locally (not cleaned)',
    )

    parser.add_argument(
        '--info',
        action='store_true',
        help='Provide detailed information on a locally started build (affected by verbosity)',
    )

    parser.add_argument(
        '--list-packages',
        action='store_true',
        help='list packages from a build (affected by verbosity)',
    )

    parser.add_argument(
        '--cleanup',
        action='store_true',
        help='Definitely remove cleaned build from the database',
    )

    parser.add_argument(
        '--rebuild',
        default=None,
        help='''Either a space separated list of package to rebuild, or one of "failed", "all".
                Triggers the rebuild of the given packages, reset the stage to build stage.
                Only reverse dependencies are rebuilt, checker build are left as-is.
                Package names are case sensitive.
        '''
    )

    return parser.parse_args()


def populate_config():
    """Get config from file if given and initialize default values"""
    home_path = Path(Path.home() / '.mpb')
    home_path.mkdir(parents=True, exist_ok=True)

    args = parse_args()
    config = get_config(args.config)

    config['backend'] = args.backend or config.setdefault('backend', 'copr')
    config['build_id'] = args.buildid or config.setdefault('build_id', 0)
    config.setdefault('base_build_id', 0)
    if args.stage is not None:
        config['stage'] = args.stage
    else:
        config.setdefault('stage', -1)
    config.setdefault('chroot', 'fedora-rawhide')
    config.setdefault('archs', 'x86_64')
    config.setdefault('skip_archs', '')
    config['verbose'] = args.verbose or config.setdefault('verbose', 0)
    config['cancel'] = args.cancel or config.setdefault('cancel', args.cancel)
    config['clean'] = args.clean or config.setdefault('clean', args.clean)
    config['cleanup'] = args.cleanup or config.setdefault('cleanup', args.cleanup)
    config.setdefault('data', str(Path(home_path / 'data')))
    config.setdefault('database', str(Path(home_path / 'share' / 'mpb.db')))
    config.setdefault('enable_priorities', True)
    config.setdefault('dnf_conf', '')
    config['collect'] = args.collect or config.setdefault('collect', args.collect)
    config['collect_list'] = args.collect_list or config.setdefault(
        'collect_list', 'failed'
    )
    config['list'] = args.list or config.setdefault('list', args.list)
    config['info'] = args.info or config.setdefault('info', args.info)
    config['list_packages'] = args.list_packages or config.setdefault('list_packages',
                                                                      args.list_packages)
    config.setdefault('retry', 0)
    config['validity'] = True

    if args.collect_list:
        if len(args.collect_list) > 0:
            config['collect_list_override'] = config.setdefault(
                'collect_list_override', True
            )
    else:
        config['collect_list_override'] = config.setdefault(
            'collect_list_override', False
        )
    config.setdefault('rebuild', args.rebuild)

    validate_config(config)

    # The checker build takes all the default from the main build, except the backend specific
    # configuration, which is reset by default.
    checker = {}
    if 'checker' in config:
        checker = config['checker'].copy()
    config.setdefault('checker', config.copy())

    # We don't want any packages in the checker by default
    config['checker']['packages'] = {}
    config['checker']['revdeps'] = {}

    for key in backends():
        # Let the backend handled their defaults
        config['checker'][key] = {}
        checker.setdefault(key, {})
        config['checker'][key].update(checker[key])

    return config


def main_loop():
    """Main mpb loop"""
    current_backend().start_agents()

    while not current_backend().is_completed():
        current_backend().execute_stage()
        current_backend().next_stage()

    current_backend().stop_agents()

def main():
    """Main function

    Setup argument parser, initialize default configuration, validates user
    input and execute the mass pre-build build.
    """
    logger()
    sys.excepthook = handle_exception
    threading.excepthook = handle_thread_exception

    config = populate_config()

    if config['validity'] is False:
        print('Configuration is broken.')
        release_database()
        sys.exit(1)

    if config['verbose'] >= 2:
        print(f'{__file__} v{VERSION}')
    else:
        print(f'{Path(sys.argv[0]).name} v{VERSION}')

    if 0 < config['verbose'] <= 2:
        print(f'Using {config["backend"]} back-end')

    print_config(config)

    list_builds(config)
    clean_builds(config)

    if any([config['list'], config['cleanup']]):
        release_database()
        return

    data_path = Path(config['data'])
    data_path.mkdir(parents=True, exist_ok=True)

    load_repos(config)

    try:
        current_backend(config)
    except ValueError as exc:
        print(exc)
        release_database()
        sys.exit(1)

    # By default 'collect' is populated from the database
    # Override this value if the user asked for it
    if config['collect_list_override']:
        current_backend().collect = config['collect_list']

    infos(config)
    list_packages(config)
    collect(config)
    cancel(config)
    clean(config)
    rebuild(config)

    if any([config['info'],
            config['list_packages'],
            config['cancel'],
            config['clean'],
            config['collect'],
            ]):
        release_database()
        return

    main_loop()

    release_database()

if __name__ == '__main__':
    main()
