""" COPR back-end implementation

    This module implements the COPR specific functionalities for the mass
    pre-builder.
"""

from datetime import datetime, timedelta
import os
from pathlib import Path
import subprocess
import threading
import yaml

from copr.v3 import Client, exceptions, config_from_file

from .package import MpbPackage, ALL_DEP_PKG
from .backend import MpbBackend

CONNECTION_TRIALS = 500

arch_table = {
    'aarch64': {'default': 'aarch64'},
    'i386': {'default': 'x86_64', 'fedora': 'i386'},
    'i586': {'default': 'x86_64', 'fedora': 'i386'},
    'i686': {'default': 'x86_64', 'fedora': 'i386'},
    'ppc64le': {'default': 'ppc64le'},
    's390x': {'default': 's390x'},
    'x86': {'default': 'x86_64', 'fedora': 'i386'},
    'x86_64': {'default': 'x86_64'},
}

def get_accepted_chroots(base, archs, distrib):
    """Compute accepted chroot list from a chroot base,
       the architecture list and a distribution name
    """
    accepted_chroots = []
    for arch in archs:
        if arch in arch_table:
            if distrib in arch_table[arch]:
                accepted_chroots.append(f'{base}-{arch_table[arch][distrib]}')
            else:
                accepted_chroots.append(f'{base}-{arch_table[arch]["default"]}')

    return accepted_chroots


class MpbCoprPackage(MpbPackage):
    """Mass pre-build copr specific package implementation

    Attributes:
        pkg_id: The package identifier for cross referencing between packages
        name: The name of the package
        base_build_id:
            The identifier of the mass pre-build the package is built on
        build_id: Build ID on the infrastructure
        build_status: A dictionary of build status per supported arch
        pkg_type: The type of the package, between main and reverse dependency
        skip_archs: The set of non-supported arch (to ease filtering)
        src_type: The source type (distgit, url, file, ...)
        src: The actual source
        committish: The tag, branch, commit ID ... For distgits ant gits
        priority: The priority for build batches
        after_pkg: A package ID we need to wait for to be able to build
        with_pkg: A package ID we build together with

    """

    _build_data = None

    def _call_build_func(self, options):
        """Call the COPR build function corresponding to the source type"""
        # Note: new switch case feature from Python 3.10 can't be used as we
        # should support Python 3.9
        if self.src_type == 'git':
            return self._owner.client.build_proxy.create_from_scm(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                committish=self.committish,
                buildopts=options,
            )

        if self.src_type == 'file':
            return self._owner.client.build_proxy.create_from_file(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                buildopts=options,
            )

        if self.src_type == 'url':
            return self._owner.client.build_proxy.create_from_url(
                self._owner.client.config['username'],
                self._owner.name,
                self.src,
                buildopts=options,
            )

        # Default: distgit
        return self._owner.client.build_proxy.create_from_distgit(
            self._owner.client.config['username'],
            self._owner.name,
            self.src_pkg_name,
            committish=self.committish,
            distgit=self.src,
            buildopts=options,
        )

    def _build_generic(self):
        """Wrapper around the infrastructure specific build method"""
        accepted_chroots = get_accepted_chroots(self._owner.chroot,
                                                self.archs,
                                                self._owner.distgit)

        background = all([
            any([
                self._owner.config['copr']['background'] == 'all',
                self.pkg_type & ALL_DEP_PKG,
                ]),
            self.src_pkg_name not in self._owner.config['copr']['disable_background'],
            self.name not in self._owner.config['copr']['disable_background'],
            ])

        opts = {
            'timeout': self._owner.config['copr']['timeout'],
            'chroots': accepted_chroots,
            'background': background,
        }

        # COPR interface forbids to provide both values simultaneously (even if
        # one as a 0 value). We therefore need to choose one.
        # The with_pkg as the highest priority, as after_pkg is always set if
        # priority is non-0 and with_pkg is reset whenever after_pkg is modified.
        # See backend->_populate_packages for more details.
        if self.with_pkg:
            opts['with_build_id'] = self.with_pkg
        else:
            if self.after_pkg:
                opts['after_build_id'] = self.after_pkg

        trials = CONNECTION_TRIALS

        while trials > 0:
            try:
                pkg_build = self._call_build_func(opts)
                break
            except exceptions.CoprRequestException as exc:
                if exc.result['error'].startswith('Unable to connect'):
                    trials -= 1
                    # Clear connection cache
                    self._owner.client.base_proxy.auth.make(reauth=True)
                    if trials:
                        continue
                    raise

                trials = CONNECTION_TRIALS
                if 'after_build_id' in opts:
                    opts['after_build_id'] = 0
                if 'with_build_id' in opts:
                    opts['with_build_id'] = 0

        self._owner.reset_monitor()
        self.build_id = pkg_build['id']
        self._update_package()

    def _build_distgit(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_file(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_git(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_url(self):
        """Infrastructure specific build method"""
        self._build_generic()

    def _build_script(self):
        """Infrastructure specific build method"""
        return

    def _set_build_status(self, check_needed=True):
        """Infrastructure specific build method"""
        if not self._build_data:
            return

        status = 'PENDING'

        if self._build_data['state'] in ['succeeded', 'forked', 'skipped']:
            status = 'SUCCESS'

        if self._build_data['state'] in ['starting', 'running']:
            status = 'RUNNING'

        if self._build_data['state'] in ['failed', 'canceled']:
            if check_needed:
                status = 'CHECK_NEEDED'
            else:
                status = 'FAILED'

        if all([self._owner.verbose >= 4, self.build_status != status]):
            print(f"New status for {self.name} is {status}")

        self.build_status = status

    def check_status(self, check_needed=True):
        """Infrastructure specific build method"""
        # pylint: disable = too-many-branches
        if not self.build_id:
            return True

        if not self._owner.monitor_project():
            return False

        # In case the user as manually restarted a build for this package
        # Try to update information about the build ID
        for build in self._owner.build_list:
            if build['source_package']['name'] == self.src_pkg_name:
                if build['id'] > self.build_id:
                    self.build_id = build['id']

        # Get build data using the build ID
        for build in self._owner.build_list:
            if build['id'] == self.build_id:
                self._build_data = build
                break

        if not self._build_data:
            # Last resort, explicitly get latest build for this package
            trials = CONNECTION_TRIALS
            while trials > 0:
                try:
                    pkg = self._owner.client.package_proxy.get(
                        self._owner.client.config['username'],
                        self._owner.name,
                        self.src_pkg_name,
                        with_latest_build=True,
                    )
                    self._build_data = pkg['builds']['latest']
                    if self._build_data:
                        self.build_id = self._build_data['id']
                    else:
                        # The package exists, but there is no build: assume the user deleted it
                        self.build_id = 0
                        self.build_status = 'FAILED'
                    break
                except exceptions.CoprNoResultException:
                    # There is really nothing we can find, give up
                    self.build_status = 'FAILED'
                    break
                except exceptions.CoprRequestException as exc:
                    if exc.result['error'].startswith('Unable to connect'):
                        trials -= 1
                        # Clear connection cache
                        self._owner.client.base_proxy.auth.make(reauth=True)
                        if trials:
                            continue
                    raise

        self._set_build_status(check_needed)

        return True

    def cancel(self):
        """Infrastructure specific cancel method"""
        if not self.build_id:
            return

        trials = CONNECTION_TRIALS

        while trials > 0:
            try:
                if self.build_status in ['RUNNING', 'PENDING']:
                    self._owner.client.build_proxy.cancel(self.build_id)

                break
            except exceptions.CoprNoResultException:
                break
            except exceptions.CoprRequestException as exc:
                if exc.result['error'].startswith('Unable to connect'):
                    trials -= 1
                    # Clear connection cache
                    self._owner.client.base_proxy.auth.make(reauth=True)
                    if trials:
                        continue
                if exc.result['error'].startswith('Cannot cancel build'):
                    break
                raise

        if self.build_status != 'SUCCESS':
            self.build_status = 'FAILED'

    def collect_data(self, dest, logs_only=False):
        """Infrastructure specific data collector method"""
        print(f'Collecting data for {self.src_pkg_name:<50s}', end='\r')
        accepted_chroots = get_accepted_chroots(self._owner.chroot,
                                                self.archs,
                                                self._owner.distgit)

        build = self._owner.client.build_proxy.get(self.build_id)
        base_len = len(os.path.split(build.repo_url))
        chroots = self._owner.client.build_chroot_proxy.get_list(self.build_id)

        for chroot in chroots:
            if chroot.name not in accepted_chroots:
                continue

            if not chroot.result_url:
                chroot.result_url = f'{build.repo_url}/srpm-builds/{self.build_id:08}/'

            cmd = [
                'wget',
                '-r',
                '-nH',
                '--no-parent',
                '--reject',
                '"index.html*"',
                '-e',
                'robots=off',
                '-q',
            ]
            cmd.extend(
                [
                    '-P',
                    os.path.join(
                        dest,
                        self._owner.name
                        + "/"
                        + self.build_status
                        + "/"
                        + chroot.name
                        + "/"
                        + self.src_pkg_name,
                    ),
                ]
            )

            if logs_only:
                cmd.extend(['-R', '*.rpm'])

            cmd.extend(['--cut-dirs', str(base_len + 2)])
            cmd.append(chroot.result_url)
            subprocess.call(cmd)

    def clean(self):
        """Infrastructure specific clean method"""
        trials = CONNECTION_TRIALS

        while trials > 0:
            try:
                if self.build_id:
                    self._owner.client.package_proxy.delete(
                        self._owner.client.config['username'],
                        self._owner.name,
                        self.src_pkg_name,
                    )
                    self.build_id = 0
                    self.build_status = 'FREE'
                break
            except exceptions.CoprNoResultException:
                self.build_id = 0
                self.build_status = 'FREE'
            except exceptions.CoprRequestException as exc:
                if exc.result['error'].startswith('Unable to connect'):
                    trials -= 1
                    # Clear connection cache
                    self._owner.client.base_proxy.auth.make(reauth=True)
                    if trials:
                        continue
                raise


class MpbCoprBackend(MpbBackend):
    """Mass pre-build COPR back-end

        Implement COPR specific functionalities.

    Attributes:
        client: The COPR client instance

    """
    def _init_copr_config(self, config):
        default_conf = {}
        config.setdefault('copr', {})
        config['copr'].setdefault('config', '')
        config.setdefault('checker', {})
        config['checker'].setdefault('copr', {})

        files = Path('/etc/mpb/copr.conf.d/').glob('*')

        for fpath in files:
            if fpath.is_file():
                with open(str(fpath), 'r', encoding='utf-8') as file:
                    default_conf.update(yaml.safe_load(file))

        if all([not default_conf, self.verbose > 0]):
            print(f'{self.name} Default configurations not found in /etc/mpb/copr.conf.d/')

        files = Path(Path.home() / '.mpb' / 'copr.conf.d').glob('*')
        for fpath in files:
            if fpath.is_file():
                with open(str(fpath), 'r', encoding='utf-8') as file:
                    if self.verbose > 0:
                        print(f'{self.name} Loading {str(fpath)}')
                    default_conf.update(yaml.safe_load(file))

        if not default_conf:
            raise FileNotFoundError('Default COPR configurations for MPB not found')

        checker_conf = default_conf.copy()
        # Update the default configuration with user inputs
        default_conf.update(config['copr'])
        checker_conf.update(config['checker']['copr'])

        # Now save the new configuration
        config['copr'] = default_conf
        config['checker']['copr'] = checker_conf

        accepted = ['all', 'revdeps']
        config['copr']['background'] = config['copr']['background'].lower()
        if config['copr']['background'] not in ['all', 'revdeps']:
            bg_val = config['copr']['background']
            raise ValueError(f'copr: background: wrong value {bg_val}, should be in {accepted}')

        if isinstance(config['copr']['disable_background'], str):
            config['copr']['disable_background'] = config['copr']['disable_background'].split()

        timeout = config['copr']['timeout']
        if int(timeout) < 0:
            print(f'Timeout is forced to 115200 due to invalid value {timeout}')
            timeout = 115200

        config['copr']['timeout'] = f'{timeout}'

    def __init__(self, database, config):
        """Call the super class, and initialize the COPR client"""
        super().__init__(database, config)
        copr_conf = config_from_file()

        self._init_copr_config(self.config)
        if self.config['copr']['config']:
            path = Path(self.config['copr']['config'])
            if path.is_file():
                copr_conf = config_from_file(str(path))
                self.config['checker']['copr']['config'] = self.config['copr']['config']

        self.client = Client(copr_conf)
        self.build_list = None
        self._monitoring = threading.Lock()
        self._next_check = 0
        self.reset_monitor()
        self.prepared = False

        # Check that connection works in principle
        trials = 5
        while trials > 0:
            # Clear connection cache
            self.client.base_proxy.auth.make(reauth=True)
            try:
                self.client.base_proxy.auth_check()
                break
            except exceptions.CoprRequestException as exc:
                if all([exc.result['error'].startswith('Unable to connect'), trials]):
                    trials -= 1
                    if trials > 0:
                        continue
                raise ValueError(f'Failed to connect: {exc.result["error"]}') from exc

    def _get_package_class(self):
        """Return the package class to be used internally"""
        return MpbCoprPackage

    def prepare(self):
        """Setup the COPR project and call super class method"""
        if self.prepared:
            return

        accepted_chroots = get_accepted_chroots(self.chroot, self.archs, self.distgit)
        trials = CONNECTION_TRIALS

        while trials > 0:
            # Create a new project in COPR for this mass rebuild
            try:
                self.client.project_proxy.add(
                    self.client.config['username'],
                    self.name,
                    accepted_chroots,
                    description=self.config['copr']['description'],
                    instructions=self.config['copr']['instructions'],
                    homepage=self.config['copr']['homepage'],
                    contact=self.config['copr']['contact'],
                    additional_repos=self.config['copr']['additional_repos'],
                    unlisted_on_hp=self.config['copr']['unlisted_on_hp'],
                    enable_net=self.config['copr']['enable_net'],
                    persistent=self.config['copr']['persistent'],
                    auto_prune=self.config['copr']['auto_prune'],
                    use_bootstrap_container=self.config['copr']['use_bootstrap_container'],
                    devel_mode=self.config['copr']['devel_mode'],
                    delete_after_days=self.config['copr']['delete_after_days'],
                    multilib=self.config['copr']['multilib'],
                    module_hotfixes=self.config['copr']['module_hotfixes'],
                    bootstrap=self.config['copr']['bootstrap'],
                    bootstrap_image=self.config['copr']['bootstrap_image'],
                    isolation=self.config['copr']['isolation'],
                    fedora_review=self.config['copr']['fedora_review'],
                    appstream=self.config['copr']['appstream'],
                    runtime_dependencies=self.config['copr']['runtime_dependencies'],
                    packit_forge_projects_allowed=
                        self.config['copr']['packit_forge_projects_allowed'],
                )
            except exceptions.CoprRequestException as exc:
                if exc.result['error'].startswith('name:'):
                    break
                if exc.result['error'].startswith('Unable to connect'):
                    trials -= 1
                    # Clear connection cache
                    self.client.base_proxy.auth.make(reauth=True)
                    if trials:
                        continue

                raise

        self.prepared = True
        super().prepare()

    def clean(self):
        """Execute super class method and destroy COPR project"""
        super().clean()

        trials = CONNECTION_TRIALS
        stop_builds = False

        while trials > 0:
            if stop_builds:
                size = len(self.packages)
                for pkg in self.packages:
                    pkg.check_status(False)
                    pkg.cancel()
                    pos = self.packages.index(pkg)
                    print(f'Stopped package build {pos}/{size}: {pkg.name:<50s}', end='\r')

            try:
                self.client.project_proxy.delete(
                    self.client.config['username'], self.name
                )
                if self.verbose >= 1:
                    print(f'Deleted project {self.name}')
                break
            except exceptions.CoprNoResultException as exc:
                print(exc)
                break
            except exceptions.CoprRequestException as exc:
                if exc.result['error'].startswith('Unable to connect'):
                    trials -= 1
                    # Clear connection cache
                    self.client.base_proxy.auth.make(reauth=True)
                    if trials:
                        continue
                if exc.result['error'].startswith('You can not delete build'):
                    stop_builds = True
                    continue
                raise

    def reset_monitor(self):
        """Force monitor to get the build list on next run"""
        with self._monitoring:
            self._next_check = datetime.now() - timedelta(days=1)

    def monitor_project(self):
        """Get the whole project's package data"""
        trials = CONNECTION_TRIALS

        while trials > 0:
            with self._monitoring:
                try:
                    if datetime.now() > self._next_check:
                        self.build_list = self.client.build_proxy.get_list(
                            self.client.config['username'], self.name
                        )
                        self._next_check = datetime.now() + timedelta(seconds=10)
                    break
                except exceptions.CoprNoResultException:
                    return False
                except exceptions.CoprRequestException as exc:
                    if exc.result['error'].startswith('Unable to connect'):
                        trials -= 1
                        # Clear connection cache
                        self.client.base_proxy.auth.make(reauth=True)
                        if trials:
                            continue
                    raise

        return True
