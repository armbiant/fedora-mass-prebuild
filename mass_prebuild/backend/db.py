""" Store mass rebuild and package state

    This module provides an interface towards an sqlite3 database which stores
    information about ongoing build and their packages.
"""

from datetime import date
from pathlib import Path
import shutil
import sqlite3
from filelock import FileLock
import yaml

authorized_status_base = {
    'RUNNING',
    'SUCCESS',
    'CHECK_NEEDED',
    'FAILED',
    'UNCONFIRMED',
}
authorized_status = authorized_status_base | {
    'FREE',
    'PENDING',
    'COMPLETED',
}
authorized_collect = authorized_status_base | {'LOG'}

authorized_types = {'distgit', 'git', 'file', 'url', 'script'}
authorized_archs = {
    'all',
    'aarch64',
    'i386',
    'i586',
    'i686',
    'ppc64le',
    's390x',
    'x86',
    'x86_64',
}
all_archs = {'aarch64', 'ppc64le', 's390x', 'x86', 'x86_64'}


class MpbDb:
    """Mass pre-build database handler

        Provides helpers to manipulate the mass pre-builder database

    Attributes:
        con: The connection to the sqlite3 database
        path: The path used to create this database
        verbose: The level of verbosity

    """

    def __init__(self, path='/tmp/mpb/mpb.db', verbose=0):
        """Initialize mass pre-builder database

            Connection to the database is established, and tables are created
            if they don't exist.

        Args:
            path: A string giving the path to the database to use (it may not
            exist)
            verbose: The level of verbosity

        """
        path = Path(path)

        path.parent.mkdir(parents=True, exist_ok=True)

        self.verbose = verbose

        self.path = str(path)

        if self.verbose >= 2:
            print(f'Creating: {self.path}')

        # Serialize write operations for multi-thread support
        self._write_lock = FileLock(self.path + ".lock")

        self.con = sqlite3.connect(self.path, check_same_thread=False)
        self.con.isolation_level = None
        self.con.row_factory = sqlite3.Row

        self._update_table = [
                self._update_tables_v0,
                self._update_tables_v1,
                self._update_tables_v2,
                self._update_tables_v3,
                ]

        cur = self.con.execute("""PRAGMA user_version""")
        user_version = cur.fetchone()[0]

        if user_version > len(self._update_table):
            print(f'Database version {user_version} is unsupported')
            print(f'Supported versions: <{len(self._update_table)}')
            raise ValueError(f'Incompatible DB {self.path}')

        for version, func in enumerate(self._update_table):
            if version < user_version:
                continue

            cur = func(cur)
            cur = self.con.execute(f"""PRAGMA user_version={version + 1:d}""")
            self.con.commit()

            if self.verbose > 2:
                print(f'Migrated DB to version {version + 1}')

        cur.close()

        if self.verbose >= 4:
            print(f'Created {self.__class__.__name__} instance at 0x{id(self):x}')

    def __repr__(self):
        """Printable representation"""
        return self.path

    def __str__(self):
        """Printable representation"""
        return self.path

    def _add_column(self, cursor, column, ctype, default, table):
        """Add a missing column in one of the tables"""
        # pylint: disable = too-many-arguments
        cursor.execute(
            f"""
            SELECT count(*) > 0
                FROM pragma_table_info('{table}')
                    WHERE name='{column}';
            """
        )

        if not cursor.fetchone()[0] > 0:
            print("Adding new column to table")
            cursor.execute(
                f"""ALTER TABLE {table}
                        ADD {column} {ctype} NOT NULL DEFAULT {default};"""
            )

    def _update_tables_v0(self, cur):
        """Create initial version of the database"""
        default_arch = {'x86_64'}

        cur.execute(
            f"""CREATE TABLE IF NOT EXISTS build (
                        build_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        base_build_id INTEGER DEFAULT 0 NOT NULL,
                        name TEXT NOT NULL DEFAULT 'undef',
                        stage INTEGER NOT NULL DEFAULT 0,
                        state TEXT NOT NULL DEFAULT 'FREE'
                            CHECK (state IN ({str(authorized_status).strip('{}')})),
                        archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        chroot TEXT DEFAULT 'fedora-rawhide' NOT NULL,
                        data TEXT DEFAULT '' NOT NULL
                        );
                """
        )

        cur.execute(
            f"""CREATE TABLE IF NOT EXISTS package (
                        pkg_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        name TEXT NOT NULL,
                        base_build_id INTEGER DEFAULT 0 NOT NULL,
                        build_id INTEGER DEFAULT 0 NOT NULL,
                        build_status TEXT
                            CHECK (build_status IN ({str(authorized_status).strip('{}')})),
                        pkg_type INTEGERS DEFAULT 0
                            CHECK (pkg_type IN (0, 1)),
                        archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        skip_archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        src_type TEXT DEFAULT 'distgit' NOT NULL
                            CHECK (src_type IN ({str(authorized_types).strip('{}')})),
                        src TEXT DEFAULT '',
                        committish TEXT DEFAULT '',
                        priority INTEGER DEFAULT 0 NOT NULL,
                        after_pkg INTEGER DEFAULT 0 NOT NULL,
                        with_pkg INTEGER DEFAULT 0 NOT NULL
                        );
                """
        )

        return cur

    def _update_tables_v1(self, cur):
        """Database v1"""
        self._add_column(cur, 'enable_priorities', 'BOOLEAN', 1, 'build')
        self._add_column(cur, 'dnf_conf', 'TEXT', '""', 'build')
        default = yaml.dump(['fail'])
        self._add_column(cur, 'collect', 'TEXT', f'"{default}"', 'build')
        default = yaml.dump([])
        self._add_column(cur, 'config', 'TEXT', f'"{default}"', 'build')

        return cur

    def _update_tables_v2(self, cur):
        default_arch = {'x86_64'}

        self.con.commit()
        self.con.close()

        ext = f'v1.{date.today()}'
        path = Path(f'{self.path}.{ext}')
        if not path.exists():
            print(f'Saving database to {self.path}.{ext} as update may be destructive')
            shutil.copy2(self.path, f'{self.path}.{ext}')

        self.con = sqlite3.connect(self.path, check_same_thread=False)
        self.con.isolation_level = None
        self.con.row_factory = sqlite3.Row

        # Backward compatibility: Remove checks to ease table modifications
        cur = self.con.execute(
            f"""CREATE TABLE IF NOT EXISTS package_new (
                        pkg_id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                        name TEXT NOT NULL,
                        base_build_id INTEGER DEFAULT 0 NOT NULL,
                        build_id INTEGER DEFAULT 0 NOT NULL,
                        build_status TEXT,
                        pkg_type INTEGERS DEFAULT 1,
                        archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        skip_archs TEXT DEFAULT '{yaml.dump(default_arch)}' NOT NULL,
                        src_type TEXT DEFAULT 'distgit' NOT NULL,
                        src TEXT DEFAULT '',
                        committish TEXT DEFAULT '',
                        priority INTEGER DEFAULT 0 NOT NULL,
                        after_pkg INTEGER DEFAULT 0 NOT NULL,
                        with_pkg INTEGER DEFAULT 0 NOT NULL
                        );
                """
        )
        cur.execute("""INSERT INTO package_new SELECT * FROM package;""")

        cur.execute("""SELECT name, pkg_id, pkg_type FROM package;""")
        pkgs = cur.fetchall()

        for pkg in pkgs:
            print(f'Updating database entry for: {pkg["name"]:100}', end='\r')
            cur.execute(
                    """UPDATE package_new
                            SET pkg_type = ?
                                WHERE pkg_id = ?;
                    """,
                    (1 << pkg['pkg_type'], pkg['pkg_id'])
                    )

        cur.execute("""DROP TABLE package;""")
        cur.execute("""ALTER TABLE package_new RENAME TO package;""")

        return cur

    def _update_tables_v3(self, cur):
        """Database v3"""
        self._add_column(cur, 'skip_archs', 'TEXT', f'"{yaml.dump(set())}"', 'build')
        self._add_column(cur, 'retry', 'INTEGER', 0, 'build')
        self._add_column(cur, 'retry', 'INTEGER', 0, 'package')
        self._add_column(cur, 'retry_count', 'INTEGER', 0, 'package')

        return cur

    def release(self):
        """Destroy the mas pre-builder database object

        All pending actions are committed to the database, and the
        connection is closed.
        """
        if self.verbose >= 4:
            print(f'Destroyed {self.__class__.__name__} instance at 0x{id(self):x}')
        self.con.commit()
        self.con.close()

    @property
    def verbose(self):
        """Return current verbosity level"""
        return self._verbose

    @verbose.setter
    def verbose(self, value):
        self._verbose = max(value, 0)

    def get_stage(self, build_id):
        """Retrieve last executed stage from DB

        Args:
            build_id: ID of the mass pre-build to get the stage of

        Returns:
            An integer

        """
        cur = self.con.execute(
            """SELECT stage FROM build WHERE build_id = ?""", (build_id,)
        )
        exists = cur.fetchone()
        cur.close()

        if exists is not None:
            return exists[0]

        print(f'Build ID {build_id} not found.')
        return 0

    def set_stage(self, build_id, new_stage):
        """Set the current stage for a given mass pre-build

        Args:
            build_id: ID of the mass pre-build to set the stage of
            new_stage: The value of the stage to set

        """
        cur = self.con.execute(
            """SELECT * FROM build WHERE build_id = ?""", (build_id,)
        )
        exists = cur.fetchall()
        cur.close()

        if len(exists):
            with self._write_lock:
                cur = self.con.execute(
                    """UPDATE build
                            SET stage = ?
                                WHERE build_id = ?
                    """,
                    (new_stage, build_id),
                )
                cur.close()
        else:
            print(f'Build ID {build_id} not found.')

    def get_state(self, build_id):
        """Get the state of the build

        Args:
            build_id: ID of the mass pre-build to get the state of

        Returns:
            A string representing the current state

        """
        cur = self.con.execute(
            """SELECT state FROM build WHERE build_id = ?""", (build_id,)
        )
        exists = cur.fetchone()
        cur.close()

        if exists is not None:
            return exists[0]

        print(f'Build ID {build_id} not found.')
        return 'FREE'

    def set_state(self, build_id, new_state):
        """Set the new state of a mass pre-build build.

        Args:
            build_id:ID of the mass pre-build to set the state of
            new_state: A string representing the new state

        Raises:
            ValueError: Given string is forbidden or ID doesn't exists

        """
        if new_state not in authorized_status:
            raise ValueError(f'Unauthorized build state {new_state}.')

        cur = self.con.execute(
            """SELECT * FROM build WHERE build_id = ?""", (build_id,)
        )
        exists = cur.fetchall()
        cur.close()

        if not exists:
            raise ValueError(f'Build ID {build_id} not found.')

        with self._write_lock:
            cur = self.con.execute(
                """UPDATE build
                        SET state = ?
                            WHERE build_id = ?
                """,
                (new_state, build_id),
            )
            cur.close()

    def remove(self, build_id):
        """Remove a build and the associated checker and packages from the database"""
        with self._write_lock:
            cursor = self.con.cursor()

            checker = self.build_by_base_id(build_id)
            if checker:
                cursor.execute("""DELETE FROM package WHERE base_build_id = ?""",
                               (checker['build_id'],))

            cursor.execute("""DELETE FROM package WHERE base_build_id = ?""", (build_id,))
            cursor.execute("""DELETE FROM build WHERE base_build_id = ?""", (build_id,))

            cursor.execute("""DELETE FROM build WHERE build_id = ?""", (build_id,))

            self.con.commit()

            cursor.close()

    def new_build(self, config):
        """Creates a new entry for mass pre-build in the database

        Args:
            config: Configuration of the new entry

        Returns:
            An integer, the ID of the new entry to be used for later reference

        """
        with self._write_lock:
            cursor = self.con.cursor()
            db_save = config['database']

            if not isinstance(db_save, str):
                config['database'] = str(config['database'])

            cursor.execute(
                """INSERT INTO build (state, base_build_id, archs, chroot,
                                      data, enable_priorities, dnf_conf,
                                      collect, config, retry, skip_archs)
                        VALUES ('FREE', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                """,
                (
                    config['base_build_id'],
                    yaml.dump(config['archs']),
                    config['chroot'],
                    config['data'],
                    config['enable_priorities'],
                    config['dnf_conf'],
                    yaml.dump(config['collect_list']),
                    yaml.dump(config),
                    config['retry'],
                    yaml.dump(config['skip_archs']),
                ),
            )
            self.con.commit()

            # Restore the database
            config['database'] = db_save

            build_id = cursor.lastrowid

            if 'name' in config:
                name = config['name']
            else:
                name = f'mpb.{build_id}'

            cursor.execute(
                """UPDATE build
                        SET name = ?
                            WHERE build_id = ?
                """,
                (name, build_id),
            )
            self.con.commit()

            cursor.close()
        return build_id

    def update_package(self, pkg_data):
        """Update a package entry or create one if it doesn't exist

        Args:
            pkg_data: The data of the package to be updated or created

        Returns:
            An integer representing the ID of the package for later reference

        """
        cursor = self.con.cursor()

        with self._write_lock:
            if pkg_data['pkg_id']:
                cursor.execute(
                    """ REPLACE INTO package (pkg_id, name, base_build_id, build_id,
                                              build_status, pkg_type, archs, skip_archs,
                                              src_type, src, committish, priority, after_pkg,
                                              with_pkg, retry, retry_count)
                                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    """,
                    (
                        pkg_data['pkg_id'],
                        pkg_data['name'],
                        pkg_data['base_build_id'],
                        pkg_data['build_id'],
                        pkg_data['build_status'],
                        pkg_data['pkg_type'],
                        yaml.dump(pkg_data['archs']),
                        yaml.dump(pkg_data['skip_archs']),
                        pkg_data['src_type'],
                        pkg_data['src'],
                        pkg_data['committish'],
                        pkg_data['priority'],
                        pkg_data['after_pkg'],
                        pkg_data['with_pkg'],
                        pkg_data['retry'],
                        pkg_data['retry_count'],
                    ),
                )
                self.con.commit()
                cursor.close()
                return pkg_data['pkg_id']

        with self._write_lock:
            cursor.execute(
                """INSERT INTO package (name, base_build_id, build_id, build_status,
                                        pkg_type, archs, skip_archs, src_type, src,
                                        committish, priority, after_pkg, with_pkg,
                                        retry, retry_count)
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                """,
                (
                    pkg_data['name'],
                    pkg_data['base_build_id'],
                    pkg_data['build_id'],
                    pkg_data['build_status'],
                    pkg_data['pkg_type'],
                    yaml.dump(pkg_data['archs']),
                    yaml.dump(pkg_data['skip_archs']),
                    pkg_data['src_type'],
                    pkg_data['src'],
                    pkg_data['committish'],
                    pkg_data['priority'],
                    pkg_data['after_pkg'],
                    pkg_data['with_pkg'],
                    pkg_data['retry'],
                    pkg_data['retry_count'],
                ),
            )
            self.con.commit()
            new_id = cursor.lastrowid
            cursor.close()
            return new_id

    def builds(self):
        """Return all available builds"""
        cursor = self.con.execute("""SELECT * FROM build ORDER BY build_id ASC;""")
        builds = cursor.fetchall()
        cursor.close()

        return builds

    def build_by_id(self, build_id):
        """Return an sqliteRow containing the build data"""
        cursor = self.con.execute(
            """SELECT * FROM build
                    WHERE build_id = ?
            """,
            (build_id,),
        )
        build = cursor.fetchone()
        cursor.close()
        return build

    def build_by_base_id(self, base_build_id):
        """Return a list of sqliteRow containing build data"""
        cursor = self.con.execute(
            """SELECT * FROM build
                    WHERE base_build_id = ?
            """,
            (base_build_id,),
        )
        build = cursor.fetchone()
        cursor.close()
        return build

    def packages_by_base_build_id(self, base_build_id):
        """Return a list of sqliteRow containing package data"""
        return self.con.execute(
            """SELECT pkg_id, name FROM package
                    WHERE base_build_id = ?
            """,
            (base_build_id,),
        ).fetchall()

    def package_by_id(self, pkg_id):
        """Return a sqliteRow containing package data"""
        cursor = self.con.execute(
            """SELECT * FROM package
                    WHERE pkg_id = ?
            """,
            (pkg_id,),
        )
        ret = cursor.fetchone()
        cursor.close()
        return ret


if __name__ == '__main__':
    # Let's do some basic testing
    # Proper testing should come, soon or later
    db = MpbDb(verbose=5)

    db.new_build({'base_build_id': -1, 'archs': {'x86_64'}, 'chroot': 'fedora-rawhide'})
    db.new_build({'base_build_id': 1, 'archs': {'aarch64'}, 'chroot': 'fedora-rawhide'})
    last_id = db.new_build(
        {'base_build_id': 2, 'archs': {'armhl'}, 'chroot': 'fedora-rawhide'}
    )

    print(db.con.execute("""SELECT * FROM build""").fetchall())

    if last_id is None:
        print('Failed to insert new build.')
    else:
        db.new_build({'base_build_id': last_id, 'archs': {'x86_64'}, 'chroot': 'rhel9'})

    db.mark_free(1)
    db.mark_failed(1)
    db.mark_pending(2)
    db.mark_unknown(3)
    try:
        db.mark_unknown(6)
    except ValueError:
        print('Oops! Trying to change state of non-existing build 6.')

    print(f'Current stage for build 3: {db.get_stage(3)}')
    db.set_stage(3, 4)
    print(f'New stage for build 3: {db.get_stage(3)}')

    print([dict(row) for row in db.con.execute("""SELECT * FROM build""").fetchall()])

    dummy_pkg = {}

    dummy_pkg['name'] = 'gcc'
    dummy_pkg['base_build_id'] = 1
    dummy_pkg['build_id'] = 1234
    dummy_pkg['build_status'] = 'PENDING'
    dummy_pkg['pkg_type'] = 0
    dummy_pkg['archs'] = {'aarch64', 'x86_64'}
    dummy_pkg['skip_archs'] = set()
    dummy_pkg['src_type'] = 'distgit'
    dummy_pkg['src'] = ''
    dummy_pkg['committish'] = ''
    dummy_pkg['priority'] = 0
    dummy_pkg['after_pkg'] = 0
    dummy_pkg['with_pkg'] = 0

    dummy_pkg['pkg_id'] = db.update_package(dummy_pkg)

    print([dict(row) for row in db.packages_by_base_build_id(1)])
    print([dict(db.package_by_id(dummy_pkg['pkg_id']))])

    dummy_pkg['build_status'] = 'RUNNING'

    dummy_pkg['pkg_id'] = db.update_package(dummy_pkg)

    print([dict(row) for row in db.packages_by_base_build_id(1)])
    res = db.package_by_id(dummy_pkg['pkg_id'])
    print([dict(res)])
    print(res[0])
    print(yaml.safe_load(res['archs']))

    import os

    os.remove('/tmp/mpb/mpb.db')
