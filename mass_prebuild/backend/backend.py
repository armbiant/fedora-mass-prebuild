""" Generic back-end implementation

    This module provides the main functionalities for the mass pre-builder. The
    infrastructure back-ends should only implement the infrastructure specific
    actions, the whole logic is centralized here.
"""
# pylint: disable = too-many-lines

from operator import itemgetter, attrgetter

import os
import sys
import time
import threading

import yaml

import __main__

from .db import (
    authorized_status_base,
    authorized_status,
    authorized_archs,
    authorized_collect,
    all_archs,
)
from ..utils.whatrequires import get_reverse_deps, group_to_package_list
from .package import MAIN_PKG, NO_BUILD, REV_DEP_PKG, REV_DEP_PKG_APPEND, ALL_DEP_PKG


def spinning_cursor():
    """Yields a spinning cursor"""
    while True:
        for cursor in '|/—\\':
            yield cursor


class MpbBackend:
    """Mass pre-build generic back-end

        This class implements generic functionalities for the back-ends. The
        whole logic must be in here, while only infrastructure specific
        implementation should go in the dedicated back-ends.

    Attributes:
        verbose: How much this thing is talking
        build_id: ID of this build
        name: Name of this build
        base_build_id: ID of the main build if this is a checker build
        checker_build: Reference to the checker build if this is a main build
        status: Current status of the build
        should_stop: Agents should stop working
        archs: Set of supported architectures
        packages: List of packages to be built
        chroot: Base of the chroots to be used
        stage: Currently executed stage
    """

    # pylint: disable = too-many-instance-attributes too-many-public-methods

    def __init__(self, database, config):
        """Create a new mass pre-build back-end entity

            If a build ID is given, the back-end data is retrieved from the
            database. Otherwise, the back-end is created out of the provided
            configuration.
        Args:
            config: The configuration to be used to create the back-end
        """
        self._database = database

        self._archs = set()
        self._skip_archs = set()
        self._status = 'FREE'
        self._stage = 0

        self._spinner = spinning_cursor()

        self.verbose = 0

        self.name = ''
        self.build_id = 0
        self.base_build_id = 0
        self.packages = []
        self.enable_priorities = True
        self.collect = []
        self._data = ''
        self.chroot = ''
        self._retry = 0

        self._build_list = []
        self._watch_list = []
        self._check_list = []
        self.should_stop = False
        self.checker_build = None

        self._build_event = None
        self._watch_event = None
        self._check_event = None

        self._build_thread = None
        self._watch_thread = None
        self._check_thread = None

        self._stages = [
            self.prepare,
            self.check_prepare,
            self.build,
            self.check_build,
            self.collect_data,
        ]

        self.config = {}

        self._update_attributes(config)

        self.distgit, self.committish, self.releasever = self._gitnbranch()

        self._pkg_class = self._get_package_class()
        self._init_packages(config)

    def _update_attributes(self, config):
        """Set initial values for attributes"""
        self.verbose = config['verbose']

        if config['base_build_id'] <= 0:
            config['base_build_id'] = 0

        if config['build_id'] <= 0:
            config['build_id'] = 0
            self.build_id = self._database.new_build(config)
        else:
            self.build_id = config['build_id']

        build_data = self._database.build_by_id(self.build_id)

        if not build_data:
            raise ValueError('Build ID doesn\'t match any known ID')

        self.archs = yaml.safe_load(build_data['archs'])
        self.skip_archs = yaml.safe_load(build_data['skip_archs'])
        self.enable_priorities = build_data['enable_priorities']
        self.dnf_conf = build_data['dnf_conf']
        self.collect = yaml.safe_load(build_data['collect'])

        if config['stage'] >= 0:
            self.stage = config['stage']
        else:
            self.stage = build_data['stage']

        self.status = build_data['state']

        if self.status != 'FREE':
            self.status = 'COMPLETED'

        if self.stage < len(self._stages) - 1:
            self.status = 'PENDING'

        self.name = build_data['name']

        self.base_build_id = build_data['base_build_id']

        self._data = build_data['data']

        self.chroot = build_data['chroot']

        self._retry = build_data['retry']

        self._reconstruct_config()

        temp_conf = yaml.safe_load(build_data['config'])
        if isinstance(temp_conf, dict):
            del config['data']
            del config['dnf_conf']
            temp_conf.update(config)
            self.config = temp_conf
        else:
            self.config.update(config)

    def _reconstruct_config(self):
        self.config = {
                'build_id': self.build_id,
                'base_build_id': self.base_build_id,
                'archs': yaml.dump(self.archs),
                'verbose': self.verbose,
                'enable_priorities': self.enable_priorities,
                'dnf_conf': self.dnf_conf,
                'collect_list': self.collect,
                'name': self.name,
                'chroot': self.chroot,
                'data': self._data,
                'database': str(self._database),
        }

    def print_config(self):
        """Dump configuration used to create this build"""
        db_save = self.config['database']
        self.config['database'] = str(self.config['database'])
        print(f'{yaml.dump(self.config)}')
        self.config['database'] = db_save

    def print_packages(self):
        """Dump packages list"""
        print('packages:')
        for pkg in [p for p in self.packages if p.pkg_type & MAIN_PKG]:
            pkg.info()

        plist = [p for p in self.packages if p.pkg_type & ALL_DEP_PKG]
        if plist:
            print('revdeps:')

            plist = [p for p in self.packages if p.pkg_type & REV_DEP_PKG]
            if plist:
                print('  list:')
                for pkg in plist:
                    pkg.info()

            plist = [p for p in self.packages if p.pkg_type & REV_DEP_PKG_APPEND]
            if plist:
                print('  append:')
                for pkg in plist:
                    pkg.info()

    def info(self):
        """Print detailed information about the build"""
        # pylint: disable = too-many-statements
        self._init_checker_build()

        verbose_save = self.verbose
        self.verbose = 0

        build_id = 'Build ID'.center(16)
        max_len = max(14, len(self.name))
        name = 'Name'.center(max_len)
        backend = 'Backend'.center(16)
        state = 'Last state'.center(16)
        stage = 'Last stage'.center(16)

        print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')
        print(f'|{build_id}| {name} |{backend}|{state}|{stage}|')
        print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')

        build_id = str(self.build_id).center(16)
        name = str(self.name).center(max_len)
        backend = 'unknown'.center(16)

        if 'backend' in self.config:
            backend = self.config['backend'].center(16)

        state = self.status.center(16)
        stage = str(self.stage).center(16)
        print(f'|{build_id}| {name} |{backend}|{state}|{stage}|')
        print(f'+{16 * "-"}+-{max_len * "-"}-+{16 * "-"}+{16 * "-"}+{16 * "-"}+')

        if self.status != 'FREE':
            print('Updating package build status...')
            for pkg in self.packages:
                if not pkg.check_status(pkg.pkg_type & ALL_DEP_PKG):
                    self.status = 'FREE'
                    break

                if pkg.build_status == 'CHECK_NEEDED':
                    chk_pkg = self.checker_build.get_package(name=pkg.name)
                    if not chk_pkg:
                        continue
                    chk_pkg.check_status(check_needed=False)

                    if chk_pkg.build_status == 'FAILED':
                        pkg.build_status = 'UNCONFIRMED'

                    if chk_pkg.build_status == 'SUCCESS':
                        pkg.build_status = 'FAILED'

        self.report_build_status(pkg_type = MAIN_PKG, spinning = False)
        self.report_build_status(pkg_type = ALL_DEP_PKG, spinning = False)
        self.verbose = verbose_save

        if self.verbose > 0:
            print('')
            print('\033[1mConfiguration:\033[0m')
            print('--8<--')
            self.print_config()
            print('--8<--')

        if self.verbose > 1:
            print('')
            print('\033[1mFull package list:\033[0m')
            print('--8<--')
            self.print_packages()
            print('--8<--')

    def _get_package_class(self):
        """Return the package class to be used internally"""
        raise NotImplementedError('Package class must be set')

    def _expend_package_groups(self, dict_in):
        """Expend any group found in a package list to a list of packages"""
        dict_out = {}
        for name, pkg in dict_in.items():
            if not name.startswith('@'):
                dict_out[name] = pkg
                continue

            name = name.removeprefix('@')

            pgroup = group_to_package_list(name, self.distgit, self.releasever)

            if not pgroup:
                dict_out[name] = pkg
                continue

            for pname in pgroup:
                dict_out[pname] = pkg.copy()

        return dict_out

    def _init_packages(self, config):
        """Populate packages"""
        # pylint: disable = too-many-branches
        try:
            pkgs = self._database.packages_by_base_build_id(self.build_id)
            for pkg in pkgs:
                self._populate_packages(pkg['pkg_id'], pkg)
            if all([pkgs, self.verbose > 1]):
                print('')
        except KeyboardInterrupt:
            self.should_stop = True
            return

        if self.stage < self._stages.index(self.build):
            pkg_type = MAIN_PKG
        else:
            pkg_type = ALL_DEP_PKG

        for pkg in [p for p in self.packages if p.pkg_type & pkg_type]:
            if pkg.build_id:
                self._watch_list.append(pkg)
            else:
                self._build_list.append(pkg)

        if self.packages:
            return

        if 'packages' in config:
            self._populate_packages(pkg_dict=config['packages'])

        if 'revdeps' not in config:
            return

        if 'list' in config['revdeps']:
            try:
                self._populate_packages(
                    pkg_dict=config['revdeps']['list'], pkg_type=REV_DEP_PKG
                )
            except KeyboardInterrupt:
                print('')
                print('Package list will be corrupted.')
                self.should_stop = True
                return

        if 'append' in config['revdeps']:
            try:
                self._populate_packages(
                    pkg_dict=config['revdeps']['append'],
                    pkg_type=REV_DEP_PKG_APPEND
                )
            except KeyboardInterrupt:
                print('')
                print('Package list will be corrupted.')
                self.should_stop = True
                return

    _chroot_strings = {
        'centos-stream': {'*': 'c{}s'},
        'fedora': {'rawhide': '{}', 'eln': 'rawhide', '*': 'f{}'},
        'epel': {'*': 'el{}'},
        'mageia': {'cauldron': '{}', '*': '{}'},  # To be checked
        'openmandriva': {'cooker': 'master', 'rolling': '{}'},  # To be checked
        'opensuse': {
            'leap-15.2': '{}',
            'leap-15.3': '{}',
            'tumbleweed': '{}',
        },  # To be checked
        'oraclelinux': {'*': '{}'},  # To be checked
        'rhel': {'*': 'rhel-{}.0.0'},
    }

    def _gitnbranch(self):
        """Returns a tuple containing the default values for the distgit, the branch,
        and the release version corresponding to the build chroot.
        """
        for git, branch_names in self._chroot_strings.items():
            if self.chroot.startswith(git):
                # Chroots needs to be changed from list to a unique string
                releasever = self.chroot.replace(f'{git}-', '', 1)
                for bname, bformat in branch_names.items():
                    if bname in releasever or bname == '*':
                        branch = bformat.format(releasever)
                        break

                return git, branch, releasever

        return None, None, None

    def _set_package_defaults(self, pkg, name, pkg_type):
        """Set package default values"""
        if 'priority' not in pkg:
            pkg['priority'] = 0
        if 'archs' in pkg:
            pkg['skip_archs'] = {a for a in self.archs if a not in pkg['archs']}
        if 'skip_archs' not in pkg:
            pkg['skip_archs'] = set()
        if pkg_type & ALL_DEP_PKG:
            pkg['skip_archs'].update(self.skip_archs)
        if 'src_type' not in pkg:
            pkg['src_type'] = 'distgit'
        if 'src' not in pkg:
            pkg['src'] = self.distgit
        if 'committish' not in pkg:
            pkg['committish'] = None
        if 'retry' not in pkg:
            pkg['retry'] = self._retry
        if 'deps_only' not in pkg:
            pkg['deps_only'] = False

        pkg['pkg_id'] = 0
        pkg['name'] = name
        pkg['build_id'] = 0
        pkg['pkg_type'] = pkg_type
        pkg['after_pkg'] = 0
        pkg['with_pkg'] = 0

        if pkg['deps_only']:
            pkg['pkg_type'] |= NO_BUILD

    def _populate_packages(self, pkg_id=0, pkg_dict=None, pkg_type=MAIN_PKG):
        """Populate the package list

            The list is either created from the given dictionary, or from the
            ID of the package given. The dictionary is ignored in the later
            case.
        Args:
            pkg_id: (optional) package ID to retrieve from the database
            pkg_dict: (optional) package data to use to populate the list
            pkg_type: (optional) type for the packages between main package and
            reverse dependency
        """
        # pylint: disable = too-many-branches
        if pkg_id:
            if self.verbose > 1:
                print(f'Populating package for {pkg_dict["name"]:50}', end='\r')

            pkg_data = {
                'pkg_id': pkg_id,
                'name': '',
                'build_id': 0,
                'pkg_type': ALL_DEP_PKG,
                'skip_archs': set(),
                'src_type': 'distgit',
                'src': '',
                'committish': '',
                'priority': 0,
                'after_pkg': 0,
                'with_pkg': 0,
                'retry': 0,
            }

            self.packages.append(self._pkg_class(self._database, self, pkg_data))
            return

        if not pkg_dict:
            return

        if not isinstance(pkg_dict, dict):
            return

        pkg_dict = self._expend_package_groups(pkg_dict)

        pkg_list = []
        for name, pkg in pkg_dict.items():
            self._set_package_defaults(pkg, name, pkg_type)
            pkg_list.append(pkg)

        pkg_list = sorted(pkg_list, key=itemgetter('priority', 'name', 'pkg_type'))

        for pkg in pkg_list:
            print(f'Populating package list with {pkg["name"]:<50s}', end='\r')
            self.packages.append(
                self._pkg_class(
                    self._database,
                    self,
                    pkg,
                )
            )

        if pkg_list:
            print('')

    def get_package(self, name='', pkg_id=0):
        """Get a package by its name or ID"""
        for pkg in self.packages:
            if all([pkg.name == name, name != '']):
                return pkg
            if all([pkg.pkg_id == pkg_id, pkg_id]):
                return pkg

        return None

    def start_agents(self):
        """Start worker agents"""
        self._check_event = threading.Event()
        self._check_thread = threading.Thread(target=self._check_agent)
        self._check_thread.start()

        self._watch_event = threading.Event()
        self._watch_thread = threading.Thread(target=self._watch_agent)
        self._watch_thread.start()

        self._build_event = threading.Event()
        self._build_thread = threading.Thread(target=self._build_agent)
        self._build_thread.start()

    def stop_agents(self, join=True):
        """Stop worker agents if they exist"""
        self.should_stop = True

        if self.base_build_id:
            return

        if self.checker_build is not None:
            self.checker_build.stop_agents()

        if self._build_event is not None:
            self._build_event.set()
        if self._watch_event is not None:
            self._watch_event.set()
        if self._check_event is not None:
            self._check_event.set()

        if not join:
            return

        if self._build_thread is not None:
            self._build_thread.join()
        if self._watch_thread is not None:
            self._watch_thread.join()
        if self._check_thread is not None:
            self._check_thread.join()

    def _build_agent(self):
        """Build agent, in charge of queuing builds in the infrastructure"""
        # pylint: disable = too-many-branches
        while not self.should_stop:
            if not self._build_list:
                self._build_event.wait()

            if self.should_stop:
                break

            self._build_event.clear()

            pkg_list = sorted(self._build_list, key=attrgetter('priority', 'name', 'pkg_type'))
            if pkg_list:
                prio = next(iter(pkg_list)).priority
            else:
                prio = 0

            prio_list = [p for p in pkg_list if p.priority == prio]
            for pkg in prio_list:
                if self.verbose >= 4:
                    print(f'Processing {pkg.name}')

                # Any previous build is replaced by this one
                if pkg.build_id:
                    pkg.cancel()
                    pkg.build_status = 'PENDING'
                    # Ensure we won't pick this package in with/after look-up
                    pkg.build_id = 0

                pkg.with_pkg = self._with_after_pkg(pkg)
                if not pkg.with_pkg:
                    pkg.after_pkg = self._with_after_pkg(pkg, 'after')

                pkg.build()
                self._build_list.remove(pkg)
                self._watch_list.append(pkg)

                if self.should_stop:
                    break

                pos = prio_list.index(pkg)
                # Wake-up the watch thread every once in a while
                if pos % 50 == 0:
                    self._watch_event.set()

            self._watch_event.set()

    def next_priority(self, pkg):
        """Find the priority to set to the given package, based on other packages that are still
        running or pending. This assume that information is up to date."""
        # We can't simply increase the priority by 1, as the dynamic retry feature relies on the
        # fact that packages with lower priority aggregate over time. By simply adding 1, if the
        # user stops the tool and restart after multiple builds were done, the builds will stay
        # scattered.
        pkgs = [p for p in self.packages
                    if all([p.pkg_type & pkg.pkg_type,  p.priority > pkg.priority])]

        prio = pkg.priority + 1
        if pkgs:
            pkgs = sorted(pkgs, key=attrgetter('priority', 'name', 'pkg_type'))

        for _pkg in pkgs:
            prio = _pkg.priority
            if _pkg.build_status in ['FREE', 'PENDING', 'RUNNING']:
                break

        return prio

    def _with_after_pkg(self, pkg, wa_type='with'):
        """Look for another package to build with (or after)"""
        ret = None

        list_wa = [p for p in self.packages
                       if all([p.pkg_type & pkg.pkg_type,
                               not p.pkg_type & NO_BUILD,
                               p.build_status in ['FREE','PENDING', 'RUNNING']])]

        if list_wa:
            list_wa = sorted(list_wa,
                             key=attrgetter('priority', 'name', 'pkg_type'),
                             reverse=wa_type=='after')
            for pkg_wa in list_wa:
                if any([all([pkg_wa.priority >= pkg.priority, wa_type == 'after']),
                        all([pkg_wa.priority < pkg.priority, wa_type == 'with'])]):
                    continue
                # Nothing suitable to build with
                if all([pkg_wa.priority > pkg.priority, wa_type == 'with']):
                    break

                # Since we took as a base the complete of packages (and not just a subset of it), we
                # can pick the first we find that has a build ID, and passed the previous tests
                if pkg_wa.build_id:
                    ret = pkg_wa
                    break

        if all([self.verbose >= 4, ret]):
            print(f'{pkg.name} set to be rebuild {wa_type} {ret.name}')

        return getattr(ret, 'build_id', 0)

    def rebuild(self, pkg):
        """Trigger a new build for the given package"""
        pkg.retry_count -= 1
        if self.verbose >= 4:
            print(f'Rebuild {pkg.name}, retries: {pkg.retry - pkg.retry_count}')

        if pkg in self._check_list:
            self._check_list.remove(pkg)
        if pkg in self._watch_list:
            self._watch_list.remove(pkg)

        # Move this package to the next batch, or build directly if there isn't any other
        # batch available
        old_prio = pkg.priority
        pkg.priority = self.next_priority(pkg)
        new_prio = pkg.priority

        if self.verbose >= 4:
            print(f'Rebuild {pkg.name}: {old_prio=}, {new_prio=}')

        pkg.after_pkg = 0
        pkg.with_pkg = 0

        # Any previous build is canceled
        if pkg.build_id:
            pkg.cancel()
            pkg.build_id = 0

        pkg.build_status = 'PENDING'

        pkg.with_pkg = self._with_after_pkg(pkg)
        if not pkg.with_pkg:
            pkg.after_pkg = self._with_after_pkg(pkg, 'after')

        self._build_list.append(pkg)

        if self._build_event:
            self._build_event.set()

    def _watch_walk_through(self, pkg_list):
        """Walk through a given list of packages to update their status"""
        # Don't execute any actions on the packages before the whole list status is up to date.
        for pkg in pkg_list:
            # Checking status can be time consuming,
            # check if we shouldn't break the loop
            if self.should_stop:
                break

            pkg.check_status(pkg.pkg_type & ALL_DEP_PKG)

        for pkg in pkg_list:
            if self.should_stop:
                break

            if all([pkg.build_status in ['FAILED', 'CHECK_NEEDED'],
                    pkg.can_retry([p for p in self.packages if p.pkg_type & pkg.pkg_type])]):
                self.rebuild(pkg)
                continue

            if pkg.build_status in ['FAILED', 'SUCCESS', 'UNCONFIRMED']:
                self._watch_list.remove(pkg)
                continue

            if pkg.build_status == 'CHECK_NEEDED':
                self._watch_list.remove(pkg)
                self._check_list.append(pkg)
                continue

            # Wake-up the checker thread every once in a while
            if pkg_list.index(pkg) % 100 == 0:
                self._check_event.set()

    def _watch_agent(self):
        """Watch ongoing builds"""
        # Let's make a walk through all packages being watched
        # to improve user experience when restarting a build
        # after a while.
        pkg_list = sorted(self._watch_list, key=attrgetter('priority', 'name', 'pkg_type'))
        self._watch_walk_through(pkg_list)

        while not self.should_stop:
            if self._watch_list:
                self._watch_event.wait(5)
            else:
                self._watch_event.wait()

            if self.should_stop:
                break

            self._watch_event.clear()

            pkg_list = sorted(self._watch_list, key=attrgetter('priority', 'name', 'pkg_type'))
            if pkg_list:
                prio = next(iter(pkg_list)).priority
            else:
                prio = 0

            prio_list = [p for p in pkg_list if p.priority <= prio]
            self._watch_walk_through(prio_list)

            self._check_event.set()

    def _init_checker_build(self):
        """Create the checker build instance"""
        if self.checker_build is not None:
            return

        if self.base_build_id != 0:
            return

        config = self.config['checker']
        config['database'] = str(self._database)
        config['name'] = f'{self.name}.checker'
        config['base_build_id'] = self.build_id

        # Ensure collect list has the values that we could find in the database
        config['collect_list'] = []
        for col in authorized_collect:
            if f'control-{col.lower()}' in self.collect:
                config['collect_list'].append(col.lower())

        exists = self._database.build_by_base_id(self.build_id)
        if exists is not None:
            config['build_id'] = exists['build_id']

        self.checker_build = self.__class__(self._database, config)
        # Inherit any change in the collect config from the parent
        self.checker_build.collect = config['collect_list']

    def _check_agent(self):
        """Initiate check builds for failed packages and watch on their status"""
        self._init_checker_build()

        while not self.should_stop:
            if self._check_list:
                self._check_event.wait(5)
            else:
                self._check_event.wait()

            if self.should_stop:
                break

            self._check_event.clear()

            to_remove = []
            for pkg in self._check_list:
                # Checking status can be time consuming,
                # check if we shouldn't break the loop
                if self.should_stop:
                    break

                chk_pkg = self.checker_build.get_package(name=pkg.name)

                if chk_pkg is None:
                    pkg_data = {
                        'pkg_id': 0,
                        'name': pkg.name,
                        'build_id': 0,
                        'pkg_type': pkg.pkg_type,
                        'skip_archs': pkg.skip_archs,
                        'src_type': pkg.src_type,
                        'src': pkg.src,
                        'committish': pkg.committish,
                        'priority': 0,
                        'after_pkg': 0,
                        'with_pkg': 0,
                        'retry': self._retry,
                    }

                    self.checker_build.packages.append(
                        self._pkg_class(
                            self._database,
                            self.checker_build,
                            pkg_data,
                        )
                    )
                    chk_pkg = self.checker_build.get_package(name=pkg.name)

                if chk_pkg.build_id == 0:
                    self.checker_build.prepare()
                    chk_pkg.build()

                chk_pkg.check_status(check_needed=False)

                if chk_pkg.build_status == 'FAILED':
                    pkg.build_status = 'UNCONFIRMED'
                    to_remove.append(pkg)

                if chk_pkg.build_status == 'SUCCESS':
                    pkg.build_status = 'FAILED'
                    to_remove.append(pkg)

            for pkg in to_remove:
                self._check_list.remove(pkg)

    def report_build_status(self, pkg_type=MAIN_PKG, spinning=True):
        """Print a simple report on the build status per package and give an
        overall status of the build

        Args:
            pkg_type:

        Returns:
            True if all package builds are done, False otherwise

        """
        # pylint: disable = too-many-branches
        done = 0
        pending = 0
        running = 0
        success = 0
        check_needed = 0
        failed = 0
        unconfirmed = 0

        packages = [p for p in self.packages if p.pkg_type & pkg_type]

        for pkg in packages:
            if pkg.build_status in ['RUNNING']:
                running += 1
            if any([
                pkg.build_status in ['FREE', 'PENDING'],
                all([pkg.build_status in ['FAILED'], pkg.retry_count != 0]),
                all([pkg.build_status in ['CHECK_NEEDED'], pkg.retry_count != 0]),
                ]):
                pending += 1
            if pkg.build_status in ['SUCCESS']:
                success += 1
                done += 1
            if all([pkg.build_status in ['FAILED'], pkg.retry_count == 0]):
                failed += 1
                done += 1
            if pkg.build_status in ['UNCONFIRMED']:
                unconfirmed += 1
                done += 1
            if all([pkg.build_status in ['CHECK_NEEDED'], pkg.retry_count == 0]):
                check_needed += 1

        if self.should_stop:
            return False

        ptype = 'main packages'
        if not pkg_type & MAIN_PKG:
            ptype = 'reverse dependencies'

        spinner = ''
        if spinning:
            spinner = f' {next(self._spinner)}'

        if len(packages):
            if self.verbose < 4:
                print(f'Build status of {ptype}:{spinner}')
                print(f'\t{done} out of {len(packages)} builds are done.')
                print(f'\tPending: {pending:<10}')
                print(f'\tRunning: {running:<10}')
                print(f'\tSuccess: {success:<10}')
                print(f'\tUnder check: {check_needed:<10}')
                print(f'\tManual confirmation needed: {unconfirmed:<10}')
                print(f'\tFailed: {failed:<10}')

            if done < len(packages):
                return False

        return True

    def prepare(self):
        """Prepare the infrastructure with the main packages"""
        self.stage = self._stages.index(self.prepare)
        if self.verbose >= 1:
            print(
                f'Executing stage {self.stage} ({self._stages[self.stage].__name__ })'
            )

        if self.verbose >= 3:
            plist = ', '.join([p.name for p in self.packages if p.pkg_type & MAIN_PKG])
            print(f'Preparing mass rebuild {self.name} on {self.chroot} for: {plist}')

        self.status = 'PENDING'

        if not self.base_build_id:
            pkgs = [p for p in self.packages if p.pkg_type & MAIN_PKG]
            # Re-init retrials
            for pkg in pkgs:
                pkg.retry_count = pkg.retry

            self._build_list += pkgs
            self._build_event.set()

            self.status = 'RUNNING'

        if self.verbose > 0:
            print(f'Prepared build {self.name} (ID: {self.build_id})')

    def check_prepare(self):
        """Check if the main packages are being built without errors and print
        a simple report"""
        self.stage = self._stages.index(self.check_prepare)
        if self.verbose >= 1:
            print(f'Executing stage {self.stage} ({self._stages[self.stage].__name__})')

        if self.verbose >= 0:
            app = os.path.basename(__main__.__file__)
            print(f'Checking build for {self.name} (ID: {self.build_id})')
            print('You can now safely interrupt this stage')
            print('Restart it later using one of the following commands:')
            print(f'"{app} --buildid {self.build_id}"')
            print(f'"{app} --buildid {self.build_id} --stage {self.stage}"')

        try:
            while not any([self.report_build_status(), self.should_stop]):
                time.sleep(1)
                if all([not self.should_stop, self.verbose < 4]):
                    sys.stdout.write('\x1b[1A' * 8)
        except KeyboardInterrupt:
            self.should_stop = True
            return

        for pkg in [p for p in self.packages if p.pkg_type & MAIN_PKG]:
            if pkg.build_status == 'FAILED':
                self.status = 'FAILED'
                print('Preparation stage failed.')
                self.collect_data()
                break

    def build(self):
        """Calculate the reverse dependencies if they are not already provided
        and queue them for building"""
        self.stage = self._stages.index(self.build)
        if self.verbose >= 1:
            print(
                f'Executing stage {self.stage} ({self._stages[self.stage].__name__ })'
            )

        if self.verbose >= 3:
            plist = ', '.join(
                [p.name for p in self.packages if p.pkg_type & ALL_DEP_PKG]
            )
            print(f'Starting mass rebuild {self.name} on {self.chroot} for: {plist}')

        if not [p for p in self.packages if p.pkg_type & REV_DEP_PKG]:
            if self.verbose >= 1:
                print('Calculating reverse dependencies.')
            try:
                rev_deps = get_reverse_deps(
                    [p.name for p in self.packages if p.pkg_type & MAIN_PKG],
                    self.distgit,
                    self.releasever,
                    self.archs,
                    priorities=self.enable_priorities,
                )
            except KeyboardInterrupt:
                print('')
                self.should_stop = True
                return

            try:
                self._populate_packages(pkg_dict=rev_deps, pkg_type=REV_DEP_PKG)
            except KeyboardInterrupt:
                print('')
                print('Package list will be corrupted.')
                self.should_stop = True
                return

        self.status = 'RUNNING'

        pkgs = [p for p in self.packages if p.pkg_type & ALL_DEP_PKG]
        # Re-init retrials
        for pkg in pkgs:
            pkg.retry_count = pkg.retry

        self._build_list += pkgs
        self._build_event.set()

    def check_build(self):
        """Check the build status for the reverse dependencies and print a
        simple report"""
        self.stage = self._stages.index(self.check_build)
        if self.verbose >= 1:
            print(
                f'Executing stage {self.stage} ({self._stages[self.stage].__name__ })'
            )

        if self.verbose >= 0:
            app = os.path.basename(__main__.__file__)
            print(f'Checking build for {self.name} (ID: {self.build_id})')
            print('You can now safely interrupt this stage')
            print('Restart it later using one of the following commands:')
            print(f'"{app} --buildid {self.build_id}"')
            print(f'"{app} --buildid {self.build_id} --stage {self.stage}"')
        try:
            while not any([self.report_build_status(ALL_DEP_PKG), self.should_stop]):
                time.sleep(1)
                if all([not self.should_stop, self.verbose < 4]):
                    sys.stdout.write('\x1b[1A' * 8)
        except KeyboardInterrupt:

            self.should_stop = True

    def collect_data(self):
        """For each packages, collect data from the remote servers"""
        self.stage = self._stages.index(self.collect_data)
        data_collected = False
        if self.verbose >= 1:
            print(f'Executing stage {self.stage} ({self._stages[self.stage].__name__})')

        for pkg in self.packages:
            for col in authorized_status_base:
                if all([pkg.build_status == col, col.lower() in self.collect]):
                    pkg.collect_data(self._data, 'log' in self.collect)
                    data_collected = True

        self._init_checker_build()
        if self.checker_build:
            self.checker_build.collect_data()

        self.status = 'COMPLETED'
        if data_collected:
            print('')

    def cancel(self):
        """Cancel build of checker build packages and owned packages"""
        self._init_checker_build()

        if self.checker_build:
            print('Cancel checker\'s packages\' builds')
            self.checker_build.cancel()

        try:
            for pkg in self.packages:
                pkg.cancel()
                pos = self.packages.index(pkg) + 1
                size = len(self.packages)
                print(f'Canceled package build {pos}/{size}: {pkg.name:<50s}', end='\r')
            if self.packages:
                print('')
        except KeyboardInterrupt:
            if self.packages:
                print('')
            return

    def clean(self):
        """Cancel builds, clean packages on the infrastructure and remove the
        remote project
        """
        self._init_checker_build()
        self.status = 'FREE'

        if self.checker_build:
            print('Cleaning checker build')
            self.checker_build.clean()

    def execute_stage(self, start=-1):
        """Execute a given stage or the last known one"""
        if start != -1:
            self.stage = start

        if self.stage < len(self._stages):
            self._stages[self.stage]()

    @property
    def archs(self):
        """Returns the set of supported archs"""
        return self._archs

    @archs.setter
    def archs(self, new_arch):
        """Set the set of supported archs

            The set of unsupported archs is automatically updated by removing
            values from this supported arch set.
        Args:
            new_arch: The set to set

        Raises:
            TypeError: The input is not a set
            ValueError: The input contains unsupported values
        """
        if 'all' in new_arch:
            self._archs = all_archs.copy()
            return

        if not isinstance(new_arch, set):
            raise TypeError('Set of arch must be given')

        if not all(elem in authorized_archs for elem in new_arch):
            raise ValueError(f'Archs should be in {authorized_archs}')

        self._archs = new_arch.copy()

    @property
    def skip_archs(self):
        """Returns the set of supported skip_archs"""
        return self._skip_archs

    @skip_archs.setter
    def skip_archs(self, new_skip_arch):
        """Set the set of supported skip_archs

            The set of unsupported skip_archs is automatically updated by removing
            values from this supported skip_arch set.
        Args:
            new_skip_arch: The set to set

        Raises:
            TypeError: The input is not a set
            ValueError: The input contains unsupported values
        """
        if 'all' in new_skip_arch:
            raise ValueError('Can\'t skip all arches')

        if not new_skip_arch:
            self._skip_archs = set()
            return

        if not isinstance(new_skip_arch, set):
            raise TypeError('Set of skip_arch must be given')

        if not all(elem in authorized_archs for elem in new_skip_arch):
            raise ValueError(f'Skipped archs should be in {authorized_archs}')

        self._skip_archs = new_skip_arch.copy()

    @property
    def status(self):
        """Return overall build status"""
        return self._status

    @status.setter
    def status(self, new_status):
        """Set overall build status"""
        if new_status in authorized_status:
            self._status = new_status
        else:
            raise ValueError(f'status value should be in {authorized_status}')

        self._database.set_state(self.build_id, new_status)

    @property
    def stage(self):
        """Return current stage"""
        return self._stage

    @stage.setter
    def stage(self, new_stage):
        """Set new stage and store it in the database"""
        if new_stage < len(self._stages):
            self._stage = new_stage
        else:
            raise ValueError(
                f'Given stage is out of bounds (max: {len(self._stages)}).'
            )

        self._database.set_stage(self.build_id, new_stage)

    def next_stage(self):
        """Set current stage to the next valid value, or stop the process"""
        if self.should_stop:
            return

        try:
            self.stage += 1
        except ValueError:
            self.should_stop = True

    def __str__(self):
        """Internal string conversion"""
        return str(self.__class__) + ': ' + str(self.__dict__)

    def is_completed(self):
        """Return True if the current status is either FAILED or COMPLETED or
        if the process should stop"""
        return any([self.status in ['FAILED', 'COMPLETED'], self.should_stop])
