""" Retrieve reverse dependencies based on a list of packages

    This module provides facilities in order to calculate the reverse
    dependencies of a provided list of packages. The list is segregated based
    on the architecture and the corresponding distribution's package
    repositories that should be looked at.
    The module can either be used stand-alone (as a plain application) or
    imported into another module.
"""

import argparse
from math import floor
from pathlib import Path
import dnf
import koji
import yaml

from mass_prebuild import VERSION

arch_table = {
    'aarch64': {'repo_arch': 'aarch64', 'search_arch': 'aarch64'},
    'i386': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'i586': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'i686': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'ppc64le': {'repo_arch': 'ppc64le', 'search_arch': 'ppc64le'},
    's390x': {'repo_arch': 's390x', 'search_arch': 's390x'},
    'x86': {'repo_arch': 'x86_64', 'search_arch': 'i686'},
    'x86_64': {'repo_arch': 'x86_64', 'search_arch': 'x86_64'},
}


def metalinks(meta_in=None):
    """Provide custom url/metalinks"""
    if not hasattr(metalinks, 'metalinks'):
        metalinks.metalinks = {}

    if meta_in:
        metalinks.metalinks.update(meta_in)

    return metalinks.metalinks


def repos_from_file(path, verbose=0):
    """Get custom repositories out of provided path"""
    if not path:
        return

    if not Path(path).is_file():
        raise ValueError(f'Can\'t find DNF config: {path}')

    with open(path, 'r', encoding='utf-8') as file:
        metalinks(yaml.safe_load(file))
        if verbose:
            print(f'Loaded {path}')
        if verbose > 5:
            print(metalinks())


def load_default_repo_conf(verbose):
    """Load default metalinks configuration"""
    files = Path('/etc/mpb/repo.conf.d/').glob('*')
    default_conf = False

    for file in files:
        if file.is_file():
            default_conf = True
            repos_from_file(str(file), verbose)

    if not default_conf:
        print('Default configurations not found in /etc/mpb/repo.conf.d/')

    files = Path(Path.home() / '.mpb' / 'repo.conf.d').glob('*')
    for file in files:
        if file.is_file():
            default_conf = True
            repos_from_file(str(file), verbose)

    if not default_conf:
        raise FileNotFoundError('Default repo configurations for MPB not found')


def koji_session(distrib='fedora', releasever='rawhide'):
    """Global access to koji session"""
    if not hasattr(koji_session, "sess"):
        meta = metalinks()[distrib]
        if releasever in meta:
            if 'koji' in meta[releasever]:
                meta = meta[releasever]

        koji_session.sess = koji.ClientSession(meta['koji'])

    return koji_session.sess


def _add_repo(base, meta, src_type, releasever, arch):
    """Add a repo (src or bin) from meta to base"""
    # DNF repos don't have classical attributes ...
    # pylint: disable = unexpected-keyword-arg
    if f'{src_type}-repos' not in meta:
        return

    meta_base = meta['base']
    repo_arch = arch_table[arch]['repo_arch']

    for src_repo in meta[f'{src_type}-repos']:
        idx = meta[f'{src_type}-repos'].index(src_repo)

        if 'metalink' in meta_base:
            repo = dnf.repo.Repo(
                'custom_' + arch + '_' + str(idx) + '_' + src_type,
                base.conf,
            )

            repo.metalink = meta_base + src_repo.format(releasever, repo_arch)
        else:
            repo = dnf.repo.Repo(
                'custom_' + arch + '_' + str(idx) + '_' + src_type,
                base.conf,
            )

            repo.baseurl.append(meta_base + src_repo.format(repo_arch))

        base.repos.add(repo)


def _dnf_base(distrib='fedora', releasever='rawhide', arch='x86_64', srcbin='all'):
    """DNF base instances"""
    if not hasattr(_dnf_base, "sess"):
        _dnf_base.sess = {}

    if arch not in _dnf_base.sess:
        _dnf_base.sess[arch] = {}

    if srcbin not in _dnf_base.sess[arch]:
        base = dnf.Base()
        meta = metalinks()[distrib]

        if releasever in meta:
            meta = meta[releasever]

        # Setup the repositories
        for src_type in ['src', 'bin', arch]:
            _add_repo(base, meta, src_type, releasever, arch)

        # By default disable everything
        repos = base.repos.get_matching('*')
        repos.disable()

        for src_type in ['src', 'bin', arch]:
            if any([srcbin == 'all', srcbin == src_type]):
                # Enable binaries
                repos = base.repos.get_matching(f'custom*{src_type}')
                repos.enable()

        base.fill_sack(load_system_repo=False)

        _dnf_base.sess[arch][srcbin] = base

    return _dnf_base.sess[arch][srcbin]


def dnf_base(distrib='fedora', releasever='rawhide', arch='x86_64'):
    """DNF base instances for both binaries and sources"""
    return _dnf_base(distrib, releasever, arch, 'all')


def dnf_base_src(distrib='fedora', releasever='rawhide', arch='x86_64'):
    """DNF base instances for both sources only"""
    return _dnf_base(distrib, releasever, arch, 'src')


def dnf_base_bin(distrib='fedora', releasever='rawhide', arch='x86_64'):
    """DNF base instances for both binaries only"""
    return _dnf_base(distrib, releasever, arch, 'bin')


def _get_sourcerpm(pkg, sack):
    """Get the sourcerpm from a given package, whether it is a binary package or a component"""
    sourcerpm = ''

    if pkg.sourcerpm:
        sourcerpm = pkg.sourcerpm
    else:
        # Silly trick to get sourcerpm, since its not available in source packages
        provides = []
        # Walk through provides, and collect all binary packages that match them
        for prov in pkg.provides:
            provides += sack.query().available().filter(name=prov.name,
                                                        arch__neq='src',
                                                        version=pkg.version,
                                                        release=pkg.release).run()

        # From these binary packages, stop at the first that has a source name that matches
        for bin_pkg in provides:
            if bin_pkg.source_name != pkg.name:
                continue
            # Extract sourcerpm from this match and be done with it
            sourcerpm = bin_pkg.sourcerpm
            break

    return sourcerpm

def get_last_build(package, distrib='fedora', releasever='rawhide', arch='x86_64'):
    """Get the source package name and commit ID from latest version of a
    package.

    This function first retrieves the latest available package matching the
    name provided, and reconstruct the NVR of the corresponding source package.
    Based on this NVR, it asks Koji to give the build information associated to
    it, and extracts the commit ID out of this data.

    Args:
        package: The package to get the tuple from
        distrib: The distribution (fedora, epel, centos ...)
        releasever: The release version for the distribution. e.g for fedora:
        36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
        arch: The architecture for the packages (x86_64, aarch64 ...).

    Returns:
        source name, commit ID, nvr
    """
    sack = dnf_base(distrib, releasever, arch).sack

    # Priority is given to components
    pkgs = sack.query().available().filter(name=[package], arch='src', latest=1).run()

    # No component found with that name, look for providers, whether they are binary packages or
    # virtual provides
    if not pkgs:
        pkgs = sack.query().available().filter(provides=[package], arch__neq='src', latest=1).run()

    if not pkgs:
        return package, '', None

    # Prefer packages that provide the sourcerpm when available
    pkgs = sorted(pkgs, key=lambda x: (x.sourcerpm is None, x.name))

    ret = {}

    for pkg in pkgs:
        name = pkg.source_name or pkg.name

        if name in ret:
            continue

        ret[name] = {}
        ret[name]['name'] = name
        ret[name]['nvr'] = _get_sourcerpm(pkg, sack).removesuffix('.src.rpm')

    ret = ret.get(package, ret[next(iter(ret))])

    ret['committish'] = None
    try:
        build_info = koji_session(distrib, releasever).getBuild(ret['nvr'], strict=True)
        if build_info['source'] is not None:
            ret['committish'] = build_info['source'].split('#')[1]
    except koji.GenericError:
        print(f'Package {ret["nvr"]} not found in koji')

    return ret['name'], ret['committish'], ret['nvr']


def __get_reverse_deps(
    packages, distrib='fedora', releasever='rawhide', arch='x86_64'
) -> dict:
    """Returns a dict of package reverse dependencies.

    From a list of a given package, request dnf to retrieve the packages that
    require at least one of the elements of this list from its BuildRequires
    field.

    Args:
        packages: The list of packages to get reverse dependencies of.
        distrib: The distribution (fedora, epel, centos ...)
        releasever: The release version for the distribution. e.g for fedora:
        36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
        arch: The architecture for the packages (x86_64, aarch64 ...).

    Returns:
        A dict of packages that depend on the input for their build, on a
        dedicated architecture.
        These packages are either architecture independent (noarch), or specific to
        the architecture requested.
    """
    # pylint: disable= too-many-locals, too-many-statements

    search_arch = arch_table[arch]['search_arch']
    sack = dnf_base(distrib, releasever, arch).sack

    binary_pkgs = []

    for pkg_str in packages:
        # First, assume this is a binary package or a virtual provide.
        # Get a list of binary package objects:
        binary_pkgs += sack.query().available().filter(
                provides=[pkg_str], arch__neq='src', latest=1
                ).run()

        # Next, assume it is a component name.
        # Get a list of source package objects:
        source_pkgs = sack.query().available().filter(
                name=[pkg_str], arch='src', latest=1
                ).run()
        for source_pkg in source_pkgs:
            # Get a list of all binary packages from these source packages and add it to the main
            # list:
            binary_pkgs += sack.query().available().filter(
                    provides=source_pkg.provides, arch__neq='src', latest=1
                    ).run()

    # This package set will be used to filter out calculated reverse dependencies that are part of
    # the given list of packages. That may happen if we have multiple main packages that may depend
    # on each other.
    pkg_set = {p.source_name or p.name for p in binary_pkgs}

    # Now that we have the list of all binary packages to use as required set,
    # calculate the corresponding reverse dependencies.
    # By using a list of package objects, virtual provides/requires work
    # Don't filter for latest here, as that may make DNF to filter out some virtual provides.
    revdeps = sack.query().filter(requires=binary_pkgs).run()

    # Prefer packages that provide the sourcerpm when available
    revdeps = sorted(revdeps, key=lambda x: (x.sourcerpm is None, x.name))

    # Create a temporary set from the packages name, to give an idea to the user how many packages
    # we will walk through for architecture filtering.
    ret = {}
    candidates = set()
    candidates.update(p.source_name or p.name for p in revdeps)
    print(f'Found {len(candidates)} candidates ({len(revdeps)} packages to process).')

    for pkg in list(revdeps):
        src_name = pkg.source_name or pkg.name

        # Don't add package in reverse dependencies if
        # they are already in the main package list
        if src_name in pkg_set:
            continue

        # We already have the source in our list.
        # Don't process sub-packages, their dependencies will be added in the deps
        # list when the component source package is processed.
        if src_name in ret:
            continue

        srpm = _get_sourcerpm(pkg, sack)

        # Reconstruct source package name based on NVR, and use it in order to
        # check if there are packages generated for the required architecture
        bin_pkg_noarch = sack.query().available().filter(sourcerpm=srpm, arch='noarch')
        bin_pkg = sack.query().available().filter(sourcerpm=srpm, arch=search_arch)

        bin_list = list(bin_pkg) + list(bin_pkg_noarch)

        if bin_list:
            ret[src_name] = {}
            deps = set()
            provides = set()

            main_pkg = [p for p in revdeps if p.name == src_name]
            for mpkg in list(main_pkg):
                deps.update(dep.name for dep in mpkg.regular_requires)
                provides.update(prov.name for prov in mpkg.provides)

            subpkgs = [p for p in revdeps if p.source_name == src_name]
            for subpkg in list(subpkgs):
                deps.update(dep.name for dep in subpkg.regular_requires)
                provides.update(prov.name for prov in subpkg.provides)

            ret[src_name]['deps'] = list(deps)
            ret[src_name]['provides'] = list(provides)

            if list(bin_pkg_noarch):
                ret[src_name]['arch'] = ['all']
            else:
                ret[src_name]['arch'] = [search_arch]

            if not len(ret) % 10:
                print(f'Got {len(ret)} components for {search_arch}.', end='\r')

    print(f'Got {len(ret)} components for {search_arch}.')
    return ret

def _increase_priority(pkg, pkg_dict):
    increased_prio = False

    for dep in pkg['real_deps']:
        provider = pkg_dict[dep]

        if provider['name'] == pkg['name']:
            continue

        # Break circular dependencies.
        # Use an arbitrary discriminator to flatten the dependency
        # graph. The more occurrences can be found in the
        # dependencies, the highest the priority for this package
        # is.
        if provider['occurrence'] < pkg['occurrence']:
            continue

        # For equal occurrences, we only break the loop it the package is alone in
        # its priority list
        current_prio = [p for p in pkg_dict.values() if p['priority'] == pkg['priority']]
        last_prio = [p for p in pkg_dict.values() if p['priority'] == pkg['priority'] - 1]
        if any([all([provider['occurrence'] == pkg['occurrence'], len(current_prio) == 1]),
                all([pkg['priority'] > 0, len(last_prio) == 0])]):
            continue

        if pkg['priority'] < provider['priority'] + 1:
            pkg['priority'] = provider['priority'] + 1

            increased_prio = True

    return increased_prio

def _prepare_deps(ret):
    """Prepare list for priority calculation"""
    print('Recalculate dependencies within current list')
    for pkg in ret.values():
        pkg['real_deps'] = []

        print(f'{floor(list(ret).index(pkg["name"]) * 100 /len(list(ret)))}% done', end='\r')
        for dep in pkg['deps']:
            for provider in ret.values():
                if dep in provider['provides']:
                    pkg['real_deps'] += [provider['name']]

    # Prepare the arbitrary discriminator to flatten the dependency graph
    print('Prepare discriminator for priorities calculation')
    # consider-using-dict-items: We actually want to traverse the dict twice
    # pylint: disable=consider-using-dict-items
    for name in ret:
        print(f'{floor(list(ret).index(name) * 100 /len(list(ret)))}% done', end='\r')
        for pkg in ret.values():
            if name in pkg['real_deps']:
                ret[name]['occurrence'] += 1
    # pylint: enable= consider-using-dict-items

def _set_priorities(ret):
    """Calculate package priorities based on dependency graph"""
    _prepare_deps(ret)

    print('Setting priorities')
    # Re-calc priority based on a simplified dependency map
    increased_prio = True
    current_pass = 0
    while increased_prio:
        print(f'Pass {current_pass}', end='\r')
        increased_prio = False

        for pkg in ret.values():
            if pkg['priority'] < current_pass:
                continue

            if _increase_priority(pkg, ret):
                increased_prio = True

        current_pass +=1

    # Saving memory
    for pkg in ret.values():
        del pkg['deps']
        del pkg['provides']
        del pkg['occurrence']
        del pkg['real_deps']

    # There still may be gaps in the priority list, so re-compress that.
    current_prio=0
    prev_prio=0
    for name, pkg in sorted(ret.items(), key=lambda x: x[1]['priority']):
        if pkg['priority'] > prev_prio:
            current_prio += 1

        prev_prio = pkg['priority']
        ret[name]['priority'] = current_prio

    print(f'{current_pass} pass done.')


def get_reverse_deps(
    packages,
    distrib='fedora',
    releasever='rawhide',
    arch_set=None,
    depth=0,
    priorities=True,
) -> dict:
    """Returns a dict of package reverse dependencies.

    From a list of given packages, request dnf to retrieve the packages that
    require at least one of the elements of this list from its BuildRequires
    field.
    Multiple architectures can be provided, the returned dictionary has an
    "archs" field listing all supported architectures for a dedicated package.

    The priority is informational, package with priority N likely depends on
    packages with priority N-1, unless a circular dependency is encountered.

    Args:
        packages: The list of packages to get reverse dependencies of.
        distrib: The distribution (fedora, epel, centos ...)
        releasever: The release version for the distribution. e.g for fedora:
            36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.
        archs: A set of architectures to look for the packages (x86_64, aarch64 ...).
        depth: The depth of the dependency tree to look for dependencies
            (default: 0, i.e. direct dependencies). Use it with parsimony,
            depth=2 will likely grab the whole distribution.
            e.g.: glibc depth 0: 258 packages, glibc depth 1: 15K packages.

    Returns:
        A dict of packages that depend on the input for their build. The
        dictionary has the following structure:
        ret = {
            'package_name': {
                'src_type': 'distgit',
                'priority': N, # N being any number starting from 0
                'archs': [ ... ]
                },
                ...
            }
    """
    # pylint: disable= too-many-arguments, too-many-locals, too-many-branches

    if arch_set is None:
        arch_set = {'x86_64'}

    ret = {}
    current_depth = 0
    pkg_list = [packages]

    if not packages:
        return ret

    while current_depth <= depth:
        pkg_dict = {}
        pkg_list.append([])
        for arch in arch_set:
            if depth > 0:
                print(
                    f'Level {current_depth} depth for {arch} on {distrib} {releasever}'
                )
            pkg_dict[arch] = __get_reverse_deps(
                pkg_list[current_depth], distrib, releasever, arch
            )

            for name, pkg in pkg_dict[arch].items():
                if name in ret:
                    if not any(a in ret[name]['arch'] for a in ['all', arch]):
                        ret[name]['arch'].append(arch)
                else:
                    ret[name] = {}
                    ret[name]['name'] = name
                    ret[name]['src_type'] = 'distgit'
                    ret[name]['src'] = distrib
                    ret[name]['arch'] = pkg['arch']
                    ret[name]['committish'] = '@last_build'
                    # Set default priority
                    ret[name]['priority'] = 0
                    if priorities:
                        ret[name]['deps'] = pkg['deps']
                        ret[name]['provides'] = pkg['provides']
                        ret[name]['occurrence'] = 0

                    pkg_list[current_depth + 1].append(name)

                print(f'Collected data for {len(ret)} packages.', end='\r')

            print(f'Collected data for {len(ret)} packages.')

        current_depth += 1

    if priorities:
        _set_priorities(ret)

    return ret

def group_to_package_list(group, distrib='fedora', releasever='rawhide') -> list:
    """Returns the list of packages contained in a given group

    This is used to convert groups of packages that may be given by the user.
    If the group doesn't exist, then an empty list is returned, and the
    original list will be used.

    Args:
        group: The name of the group to check for
        distrib: The distribution (fedora, epel, centos ...)
        releasever: The release version for the distribution. e.g for fedora:
            36, 37, rawhide; for centos-stream: 8, 9; for epel: 8, 9.

    Returns:
        A list of package names.
    """
    ret = []

    # Arch shouldn't matter here
    base = dnf_base(distrib, releasever, "x86_64")
    base.read_comps()

    if not base.comps:
        return ret

    ret_group = base.comps.group_by_pattern(group)

    if not ret_group:
        return ret

    for pkg in ret_group.packages_iter():
        ret.append(pkg.name)

    return ret

def main():
    """Calculate reverse dependencies for packages given in command line

    This function is the entry point for the command line interface to
    calculate the reverse dependencies of a given list of packages.

    The list is either printed to STDOUT or to a file, as requested by the
    user.
    """

    parser = argparse.ArgumentParser(
        description='A package reverse dependency collector'
    )

    parser.add_argument(
        '--verbose', '-V', action='store_true', help='increase output verbosity'
    )
    parser.add_argument(
        '--version',
        '-v',
        action='version',
        version=f'%(prog)s {VERSION}',
        help='show the version number and exit',
    )
    parser.add_argument(
        '-o', '--output', default=None, help='where to store the list'
    )

    parser.add_argument(
        '--distrib',
        default='fedora',
        help='distribution for the package repository',
    )

    parser.add_argument('-a', '--arch', default='x86_64', help='architecture')
    parser.add_argument('-d', '--depth', default=0, type=int, help='dependency depth')
    parser.add_argument(
        '-r',
        '--releasever',
        default='rawhide',
        help='release version (e.g 36, rawhide ...)',
    )

    parser.add_argument(
            '-R',
            '--repo',
            help='custom dnf configuration in a special format, see examples/repo.conf.yaml'
    )

    parser.add_argument(
        '--no-deps', action='store_true', help='only retrieve last build info'
    )

    parser.add_argument(
        '--no-priority', action='store_true', help='disable priority calculation'
    )

    parser.add_argument(
        'packages',
        nargs='+',
        type=str,
        action='extend',
        help='list of packages to collect reverse dependencies for',
    )

    args = parser.parse_args()

    if args.verbose:
        pkgs = ', '.join(args.packages)
        print(f'Collecting reverse dependencies for {pkgs}')
        print(f'Result will be provided to {args.output}')

    load_default_repo_conf(1)

    if args.repo:
        if Path(args.repo).is_file():
            repos_from_file(args.repo, 1)

    pkg_list = []
    for pkg in args.packages:
        pgroup = group_to_package_list(pkg, args.distrib, args.releasever)
        if pgroup:
            pkg_list += pgroup
        else:
            pkg_list.append(pkg)

    for pkg in pkg_list:
        name, committish, nvr = get_last_build(
            pkg, args.distrib, args.releasever, args.arch
        )
        print(f'{pkg}:{name}:{nvr} {committish}')

    if args.no_deps:
        return

    pkgs = get_reverse_deps(
        pkg_list, args.distrib, args.releasever, set([args.arch]), args.depth, not args.no_priority
    )

    if args.output:
        with open(args.output, 'w', encoding='utf-8') as file:
            print(f'{yaml.dump(pkgs)}', file=file)
    else:
        print(f'{yaml.dump(pkgs)}')


if __name__ == '__main__':
    main()
