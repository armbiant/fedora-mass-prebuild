#!/bin/bash

set -e

name=$(head -c 256 /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)

rm -f /tmp/ci-${name}.sh

for exc in "$(dirname $0)"/excerpts/*.sh "$(dirname $0)"/excerpts/before/*.sh
do
  cat ${exc} >> /tmp/ci-${name}.sh
  echo "" >> /tmp/ci-${name}.sh
done

SCRIPTS=''

if [ $# -gt 0 ]
then
  # Assume user only wants to execute some of the regression tests
  SCRIPTS="$(dirname $0)/02*/*" # Ensure mpb is installed
  SCRIPTS="${SCRIPTS} $(dirname $0)/03*/000*" # Add regression header
  for item in $@
  do
    SCRIPTS="${SCRIPTS} $(dirname $0)/03*/${item}"
  done
  SCRIPTS="${SCRIPTS} $(dirname $0)/03*/999*" # Add regression footer
else
  SCRIPTS="$(dirname $0)/*"
fi

for dir in ${SCRIPTS}
do
  [ -d ${dir} ] || continue
  [ ! "${dir}" == "$(dirname $0)/excerpts" ] || continue
  cat $(dirname $0)/$(basename $dir)/*.sh >> /tmp/ci-${name}.sh
  echo "" >> /tmp/ci-${name}.sh
done

for file in ${SCRIPTS}
do
  [ -f ${file} ] || continue
  cat ${file} >> /tmp/ci-${name}.sh
  echo "" >> /tmp/ci-${name}.sh
done

cd $(dirname $0)
cd $(git rev-parse --show-toplevel)

bash /tmp/ci-${name}.sh 2>/dev/null
