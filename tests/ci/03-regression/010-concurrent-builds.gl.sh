echo -e "${BBlue}##### Checking concurrent execution of MPB #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

rm -rf ${EXCEPTION_LOG}

revdeps=""

for i in {0..2000}
do
  revdeps="${revdeps}package${i} "
done

cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  main_package:
    retry: 10
revdeps:
  list: ${revdeps}
backend: dummy
verbose: 5
EOF

PID_LIST=""
ID_LIST=""
res=""
res1=""
res2=""

for i in {0..2}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE}.${i} 2>&1 &
  PID_LIST="${PID_LIST}$! "
  sleep 1
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE}.${i} | sed "s/\"//g" | cut -d " " -f 3 || true)
  ID_LIST="${ID_LIST}${ID} "
done

for j in {0..50}
do
  sleep 20

  for i in {0..2}
  do
    except=$(grep "Exception log stored" ${LOG_FILE}.${i} || true)
    if [ ! "x${except}" == "x" ]
    then
      cp ${LOG_FILE}.${i} ${LOG_FILE}
      FAIL
    fi
  done

  for i in {0..2}
  do
    if [ ! "x${res1}" == "x" ]
    then
      break
    fi

    res1=$(grep "Executing stage 3" ${LOG_FILE}.${i} || true)

    if [ ! "x${res1}" == "x" ]
    then
      echo -en "                                        \r"
      echo -e "${Purple}Stage 3 is ongoing${Color_Off}"
      break
    fi
  done

  for i in {0..2}
  do
    res2=$(grep "Executing stage 4" ${LOG_FILE}.${i} || true)
    if [ ! "x${res2}" == "x" ]
    then
      break
    fi
  done

  if [ ! "x${res2}" == "x" ]
  then
    # Let's assume we are done with this test
    break
  fi

  echo -en "${Purple}	Loop ${j} done  ${Color_Off}\r"
done

# Stop MPB
for PID in $PID_LIST
do
  kill -s INT ${PID} > /dev/null 2>&1 || true
done

sleep 1

for PID in $PID_LIST
do
  kill -s KILL ${PID} > /dev/null 2>&1 || true
done

sleep 1

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
then
  echo -e "${Red}No build succeeded in time${Color_Off}"
  for ID in $ID_LIST
  do
    mpb --info --buildid $ID || true
  done
  FAIL
fi

if [ -f ${EXCEPTION_LOG} ]
then
  if [ ! "$(cat ${EXCEPTION_LOG} | wc -l)" == "0" ]
  then
    echo -e "${BRed}Exception raised during tests${Color_Off}"

    for i in {0..2}
    do
      cat ${LOG_FILE}.${i} || true
    done

    cat ${EXCEPTION_LOG}
    FAIL
  fi
fi

for i in {0..2}
do
  rm -f ${LOG_FILE}.${i} || true
done

echo -e "${BGreen}OK${Color_Off}"

echo ""
