echo -e "${BBlue}##### Print help #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

mpb --help || FAIL
mpb-whatrequires --help || FAIL
mpb-failedconf --help || FAIL
mpb-copr-edit --help || FAIL

echo -e "${BGreen}OK${Color_Off}"

echo ""
