echo -e "${BBlue}##### Checking command line overwrites #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

rm -rf ${EXCEPTION_LOG}

echo -e "${Purple}Prepare a new build to check the config overwrites${Color_Off}"
# In principle, there were builds created in previous tests, but assume this
# one is executed standalone
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config
revdeps:
  list:
    autoconf
verbose: 5
backend: dummy
name: config-overwrites
EOF

res=""
# There is some probability that the main package fails for testing purpose
mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

echo -e "${Blue}Check default values${Color_Off}"

mpb --list -VVV > ${LOG_FILE} || true

SAMPLE=/tmp/config.sample
cat > ${SAMPLE} << EOF
archs: x86_64
backend: copr
base_build_id: 0
build_id: 0
cancel: false
chroot: fedora-rawhide
clean: false
cleanup: false
collect: false
collect_list: failed
collect_list_override: false
dnf_conf: ''
enable_priorities: true
info: false
list: true
list_packages: false
rebuild: null
retry: 0
skip_archs: ''
stage: -1
validity: true
verbose: 3
EOF

lines=$(grep -nE '^verbose' ${LOG_FILE} | cut -d ':' -f 1)
head ${LOG_FILE} -n $((lines)) | tail -n $((lines - 3)) | grep -vE '^data|^checker|^  ' > ${LOG_FILE}.1

diff --color=always ${LOG_FILE}.1 ${SAMPLE} > ${LOG_FILE}.diff || true

if [ ! "$(cat ${LOG_FILE}.diff)" = "" ]
then
  cat ${LOG_FILE}.1
  echo $(cat ${LOG_FILE}.diff)
  FAIL
fi

echo -e "${Blue}Check verbose overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
verbose: 1
EOF

mpb --config=${CONFIG_FILE} --list -VVVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'verbose: 4' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check build ID overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
build_id: 999999999
EOF

mpb --config=${CONFIG_FILE} --list --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q "build_id: ${ID}" ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check cancel overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
cancel: false
EOF

mpb --config=${CONFIG_FILE} --cancel --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'cancel: true' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check clean overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
clean: false
EOF

mpb --config=${CONFIG_FILE} --clean --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'clean: true' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check collect overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
collect: false
EOF

mpb --config=${CONFIG_FILE} --list --collect --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'collect: true' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check collect list overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
collect_list: control-failed
EOF

mpb --config=${CONFIG_FILE} --list --collect-list="success" --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'collect_list: success' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check info overwrite${Color_Off}"
# Test similar to 008 but with config file

cat > ${CONFIG_FILE} << EOF
info: false
EOF

mpb --config=${CONFIG_FILE} --info --buildid ${ID} -VVV > ${LOG_FILE} || true

res=$(grep "config-overwrites" ${LOG_FILE} || true)
res1=$(grep "Configuration:" ${LOG_FILE} || true)
res2=$(grep "Full package list:" ${LOG_FILE} || true)

if [ "x${res}" == "x" ] || [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Check list overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
list: false
EOF

mpb --config=${CONFIG_FILE} --list --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'list: true' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check list packages overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
list_packages: false
EOF

mpb --config=${CONFIG_FILE} --list-packages --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'list_packages: true' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check stage overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
stage: 5
EOF

mpb --config=${CONFIG_FILE} --list --stage 3 --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'stage: 3' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check stage 0 overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
stage: 5
EOF

mpb --config=${CONFIG_FILE} --list --stage 0 --buildid ${ID} -VVV > ${LOG_FILE} || true

sed -n '/Using the following config:/,/^$/{/^$/q; p}' ${LOG_FILE} > ${LOG_FILE}.1
mv ${LOG_FILE}.1 ${LOG_FILE}

if ! grep -q 'stage: 0' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check backend overwrite${Color_Off}"

cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config
revdeps:
  list:
    autoconf
verbose: 5
backend: copr
name: config-overwrites
EOF

mpb --config=${CONFIG_FILE} --backend="dummy" -V > ${LOG_FILE} || true
ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

res=$(grep "Using dummy back-end" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  mpb --clean --buildid ${ID} || true
  FAIL
fi

rm -f ${LOG_FILE} || true

if [ -f ${EXCEPTION_LOG} ]
then
  if [ ! "$(cat ${EXCEPTION_LOG} | wc -l)" == "0" ]
  then
    echo -e "${BRed}Exception raised during tests${Color_Off}"
    cat ${EXCEPTION_LOG}
    FAIL
  fi
fi

echo -e "${BGreen}OK${Color_Off}"

echo ""
