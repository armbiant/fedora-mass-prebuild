echo -e "${BBlue}##### Checking package build retrial #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

rm -rf ${EXCEPTION_LOG}

echo -e "${Blue}Global retry on main package${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages: redhat-rpm-config aucotonf automake libtool gcc glibc
revdeps:
  list:
    autoconf-archive
backend: dummy
retry: 5
verbose: 5
EOF

PID=0
res=""

for i in {0..20}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 5
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    sleep 5

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Rebuild" ${LOG_FILE} | grep -v "autoconf-archive" || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res1=$(grep "Executing stage 3" ${LOG_FILE} || true)
    if [ ! "x${res1}" == "x" ]
    then
      # We are done with this build
      break
    fi

    echo -en "${Purple}	Loop ${i}.${j} done  ${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 1

  disown ${PID} || true
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Global retry on rev-deps${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config
backend: dummy
retry: 5
verbose: 5
EOF

PID=0
res=""

for i in {0..15}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 5
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    sleep 5

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Rebuild" ${LOG_FILE} | grep -v "redhat-rpm-config" || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res1=$(grep "Executing stage 4" ${LOG_FILE} || true)
    if [ ! "x${res1}" == "x" ]
    then
      # We are done with this build
      break
    fi

    echo -en "${Purple}	Loop ${i}.${j} done  ${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 1

  disown ${PID} || true
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Disable global retry${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    retry: 0
revdeps:
  list:
    autoconf-archive:
      retry: 0
backend: dummy
retry: 5
verbose: 5
EOF

PID=0
res=""

for i in {0..20}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 5
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    sleep 5

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res1=$(grep "Rebuild " | grep "attempts left:" ${LOG_FILE} || true)
    if [ ! "x${res1}" == "x" ]
    then
      FAIL
    fi

    res=$(grep -E "New status for [a-z_-]* is FAILED" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res1=$(grep "Executing stage 4" ${LOG_FILE} || true)
    if [ ! "x${res1}" == "x" ]
    then
      # We are done with this build
      break
    fi

    echo -en "${Purple}	Loop ${i}.${j} done  ${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 1

  disown ${PID} || true
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Package specific retry on main package${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    retry: 10
  autoconf:
    retry: 10
  automake:
    retry: 10
revdeps:
  list:
    autoconf-archive
backend: dummy
retry: 5
verbose: 5
EOF

PID=0
res=""

for i in {0..20}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 5
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    sleep 5

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Rebuild redhat-rpm-config, retries: 1" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res=$(grep "Rebuild autoconf, retries: 1" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res=$(grep "Rebuild automake, retries: 1" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res1=$(grep "Executing stage 4" ${LOG_FILE} || true)
    if [ ! "x${res1}" == "x" ]
    then
      # We are done with this build
      break
    fi
    echo -en "${Purple}	Loop ${i}.${j} done  ${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 1

  disown ${PID} || true
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Package specific retry on rev deps${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config
revdeps:
  list:
    autoconf-archive:
      retry: 10
    autoconf:
      retry: 10
    automake:
      retry: 10
backend: dummy
retry: 5
verbose: 5
EOF

PID=0
res=""

for i in {0..20}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 5
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    sleep 5

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Rebuild autoconf-archive, retries: 1" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res=$(grep "Rebuild autoconf, retries: 1" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res=$(grep "Rebuild automake, retries: 1" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi

    res1=$(grep "Executing stage 4" ${LOG_FILE} || true)
    if [ ! "x${res1}" == "x" ]
    then
      # We are done with this build
      break
    fi

    echo -en "${Purple}	Loop ${i}.${j} done  ${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 1

  disown ${PID} || true
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Dynamic retry on rev deps${Color_Off}"
revdeps=""

for i in {0..50}
do
  revdeps="${revdeps}package${i} "
done

cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  main_package:
    retry: 10
revdeps:
  list: ${revdeps}
retry: dynamic
backend: dummy
verbose: 5
EOF

PID=0
res=""

for i in {0..20}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 5
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    sleep 5

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res1=$(grep "Executing stage 4" ${LOG_FILE} || true)
    if [ ! "x${res1}" == "x" ]
    then
      # We are done with this build
      break
    fi

    echo -en "${Purple}	Loop ${i}.${j} done  ${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 1

  disown ${PID} || true
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  res=$(grep -E "Rebuild package[0-9]+, retries: " ${LOG_FILE} || true)
  if [ ! "x${res}" == "x" ]
  then
    break
  fi
done

# Erase "Loop X done"
echo -en "                                        \r"

if [ "x${res}" == "x" ]
then
  FAIL
fi

rm -f ${LOG_FILE} || true

if [ -f ${EXCEPTION_LOG} ]
then
  if [ ! "$(cat ${EXCEPTION_LOG} | wc -l)" == "0" ]
  then
    echo -e "${BRed}Exception raised during tests${Color_Off}"
    cat ${EXCEPTION_LOG}
    FAIL
  fi
fi

echo -e "${BGreen}OK${Color_Off}"

echo ""
