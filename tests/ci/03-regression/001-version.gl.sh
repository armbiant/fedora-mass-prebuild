echo -e "${BBlue}##### Checking versions #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

mpb --version || FAIL
mpb-whatrequires --version || FAIL
mpb-failedconf --version || FAIL
mpb-copr-edit --version || FAIL

echo -e "${BGreen}OK${Color_Off}"

echo ""
