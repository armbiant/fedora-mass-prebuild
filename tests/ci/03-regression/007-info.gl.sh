echo -e "${BBlue}##### Checking build infos retrieval #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

echo -e "${Purple}Prepare a new build to check the list and infos commands${Color_Off}"
# In principle, there were builds created in previous tests, but assume this
# one is executed standalone
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config
revdeps:
  list:
    autoconf
verbose: 5
backend: dummy
name: check-list-info
EOF

res=""
# There is some probability that the main package fails for testing purpose
mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)
res1=$(grep "backend: dummy" ${LOG_FILE} || true)
res2=$(grep "Executing stage 4" ${LOG_FILE} || true)

if [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
then
  # General failure case
  FAIL
fi

echo -e "${Blue}Check build list${Color_Off}"
mpb --list > ${LOG_FILE} || true
res=$(grep "check-list-info" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

CHECK_ID=$(echo ${res} | tr -d ' ' | cut -d '|' -f 2)
CHECK_NAME=$(echo ${res} | tr -d ' ' | cut -d '|' -f 3)
CHECK_BACKEND=$(echo ${res} | tr -d ' ' | cut -d '|' -f 4)

if [ ! "${CHECK_ID}" == "${ID}" ] || [ ! "${CHECK_NAME}" == "check-list-info" ] || [ ! "${CHECK_BACKEND}" == "dummy" ]
then
  echo "Check failed: ${CHECK_ID}:${ID}, ${CHECK_NAME}, ${CHECK_BACKEND}"
  FAIL
fi

echo -e "${Blue}Check build info (no verbosity)${Color_Off}"
mpb --info --buildid ${ID} > ${LOG_FILE} || true
res=$(grep "check-list-info" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

CHECK_ID=$(echo ${res} | tr -d ' ' | cut -d '|' -f 2)
CHECK_NAME=$(echo ${res} | tr -d ' ' | cut -d '|' -f 3)
CHECK_BACKEND=$(echo ${res} | tr -d ' ' | cut -d '|' -f 4)

if [ ! "${CHECK_ID}" == "${ID}" ] || [ ! "${CHECK_NAME}" == "check-list-info" ] || [ ! "${CHECK_BACKEND}" == "dummy" ]
then
  FAIL
fi

res=$(grep "Build status of main packages" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Check build info (V=1)${Color_Off}"
mpb --info --buildid ${ID} -V > ${LOG_FILE} || true
res=$(grep "check-list-info" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

CHECK_ID=$(echo ${res} | tr -d ' ' | cut -d '|' -f 2)
CHECK_NAME=$(echo ${res} | tr -d ' ' | cut -d '|' -f 3)
CHECK_BACKEND=$(echo ${res} | tr -d ' ' | cut -d '|' -f 4)

if [ ! "${CHECK_ID}" == "${ID}" ] || [ ! "${CHECK_NAME}" == "check-list-info" ] || [ ! "${CHECK_BACKEND}" == "dummy" ]
then
  FAIL
fi

res=$(grep "Configuration:" ${LOG_FILE} || true)
res1=$(grep "backend: dummy" ${LOG_FILE} || true)
res2=$(grep "build_id: ${ID}" ${LOG_FILE} || true)

if [ "x${res}" == "x" ] || [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Check build info (V=2)${Color_Off}"
mpb --info --buildid ${ID} -VV > ${LOG_FILE} || true
res=$(grep "check-list-info" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

CHECK_ID=$(echo ${res} | tr -d ' ' | cut -d '|' -f 2)
CHECK_NAME=$(echo ${res} | tr -d ' ' | cut -d '|' -f 3)
CHECK_BACKEND=$(echo ${res} | tr -d ' ' | cut -d '|' -f 4)

if [ ! "${CHECK_ID}" == "${ID}" ] || [ ! "${CHECK_NAME}" == "check-list-info" ] || [ ! "${CHECK_BACKEND}" == "dummy" ]
then
  FAIL
fi

res=$(grep "Full package list:" ${LOG_FILE} || true)
res1=$(grep "packages:" ${LOG_FILE} || true)
res2=$(grep "committish: None" ${LOG_FILE} || true)

if [ "x${res}" == "x" ] || [ "x${res1}" == "x" ] || [ "x${res2}" == "x" ]
then
  FAIL
fi

rm -f ${LOG_FILE} || true

if [ -f ${EXCEPTION_LOG} ]
then
  if [ ! "$(cat ${EXCEPTION_LOG} | wc -l)" == "0" ]
  then
    echo -e "${BRed}Exception raised during tests${Color_Off}"
    cat ${EXCEPTION_LOG}
    FAIL
  fi
fi

echo -e "${BGreen}OK${Color_Off}"

echo ""
