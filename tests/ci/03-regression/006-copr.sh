echo -e "${BBlue}##### Checking COPR backend #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

rm -rf ${EXCEPTION_LOG}

echo -e "${Blue}COPR back-end (w/o rev deps)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64 i686
packages:
  redhat-rpm-config:
    committish: '@last_build'
    skip_arch: i686
revdeps:
  list:
    autoconf-archive:
      committish: '@last_build'
name: copr-short
data: /tmp/.mpb/data
collect-list: success, failed, log, checker-success, checker-failed, checker-log
backend: copr
skip_arch: x86_64
verbose: 5
EOF

PID=0
res=""

for i in {0..5}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 15
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    sleep 20

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Executing stage 4" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      # We are done with this build
      break
    fi
    echo -en "${Purple}	Loop ${j} done${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 5

  disown ${PID}
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi

  # Try to cleanup
  mpb --clean --buildid ${ID} > /dev/null 2>&1 || \
    echo -e "${BRed}Failed to clean copr-short${Color_Off}"
  echo -e "${BPurple}Trial ${i}/5 failed${Color_Off}"
done

# Erase "Loop X done"
echo -en "                                        \r"

echo -e "${Blue}Clean existing COPR${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
clean: True
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE}
res=$(grep "Cleaning build ${ID}" ${LOG_FILE} || true)
res1=$(grep "Exception log stored" ${LOG_FILE} || true)

if [ "x${res}" == "x" ] && [ ! "x${res1}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}COPR back-end (w/ rev deps)${Color_Off}"
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  autoconf-archive:
    committish: '@last_build'
verbose: 5
backend: copr
data: /tmp/.mpb/data
name: copr-long
collect-list: success, failed, log
EOF

res=""
for i in {0..5}
do
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 10

  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..30}
  do
    sleep 20

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Calculating reverse dependencies" ${LOG_FILE} || true)
    res1=$(grep "Executing stage 3" ${LOG_FILE} || true)
    res2=$(grep "Executing stage 4" ${LOG_FILE} || true)
    if [ ! "x${res1}" == "x" ] || [ ! "x${res2}" == "x" ]
    then
      # We are done with this build
      # Wait for few rev deps builds to be started
      sleep 60
      break
    fi
    echo -en "${BPurple}	Loop ${j} done${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 5

  disown ${PID}
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi

  # Try to cleanup
  mpb --clean --buildid ${ID} > /dev/null 2>&1 || \
    echo -e "${BRed}Failed to clean copr-long${Color_Off}"
  echo -e "${BPurple}Trial ${i}/5 failed${Color_Off}"
done

if [ "x${res}" == "x" ]
then
  # Try to cleanup
  mpb --clean --buildid ${ID} > /dev/null 2>&1 || \
    echo -e "${BRed}Failed to clean copr-long${Color_Off}"
  FAIL
fi

echo -e "${Blue}Cancel existing COPR${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
cancel: True
EOF

# Now we assume that any of the following MPB command may fail,
# but we want to see the logs if that happens.
mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
res=$(grep "Canceling ${ID}" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}Collect existing COPR${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
collect: True
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
res=$(grep "Collecting data for copr-long" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

tree -d -L 4 /tmp/.mpb/data

echo -e "${Blue}Clean existing COPR${Color_Off}"
cat > ${CONFIG_FILE} << EOF
build_id: ${ID}
clean: True
EOF

mpb --config=${CONFIG_FILE} > ${LOG_FILE} || true
res=$(grep "Cleaning build ${ID}" ${LOG_FILE} || true)

if [ "x${res}" == "x" ]
then
  FAIL
fi

echo -e "${Blue}COPR back-end (w/ rev deps calculation, deps-only packages)${Color_Off}"
# The dummy test verifies that the dependency list contains deps from
# autocon-archive, copr tests will check if this package is actually NOT built
cat > ${CONFIG_FILE} << EOF
archs: x86_64
packages:
  redhat-rpm-config:
    committish: '@last_build'
  autoconf-archive:
    deps_only: True
retry: 3
verbose: 5
name: copr-deps-only
backend: copr
EOF

PID=0
res=""

for i in {0..5}
do
  # In principle the command may fail if the package we are testing is broken
  # Assume a real failure would imply an exception before the end of the process
  mpb --config=${CONFIG_FILE} > ${LOG_FILE} 2>&1 &
  PID=$!

  sleep 15
  ID=$(grep -m 1 "mpb --buildid"  ${LOG_FILE} | sed "s/\"//g" | cut -d " " -f 3 || true)

  for j in {0..50}
  do
    sleep 20

    except=$(grep "Exception log stored" ${LOG_FILE} || true)
    if [ ! "x${except}" == "x" ]
    then
      FAIL
    fi

    res=$(grep "Executing stage 3" ${LOG_FILE} || true)
    if [ ! "x${res}" == "x" ]
    then
      sleep 10
      # We are done with this build
      break
    fi
    echo -en "${Purple}	Loop ${j} done${Color_Off}\r"
  done

  # Stop MPB
  kill -s INT ${PID} > /dev/null 2>&1 || true

  sleep 5

  disown ${PID}
  kill -s KILL ${PID} > /dev/null 2>&1 || true

  sleep 1

  if [ ! "x${res}" == "x" ]
  then
    break
  fi

  # Try to cleanup
  mpb --clean --buildid ${ID} > /dev/null 2>&1 || \
    echo -e "${BRed}Failed to clean copr-deps-only${Color_Off}"
  echo -e "${BPurple}Trial ${i}/5 failed${Color_Off}"
done

# Erase "Loop X done"
echo -en "                                        \r"

# Check build list with copr-cli
copr-cli list-builds copr-deps-only > ${LOG_FILE}

res=$(grep "autoconf-archive" ${LOG_FILE} || true)
res2=$(cat ${LOG_FILE} | wc -l)

mpb --clean --buildid ${ID} > /dev/null 2>&1 || \
    echo -e "${BRed}Failed to clean copr-deps-only${Color_Off}"

if [ ! "x${res}" == "x" ] || [ ${res2} -lt 2 ]
then
  FAIL
fi

rm -f ${LOG_FILE} || true

if [ -f ${EXCEPTION_LOG} ]
then
  if [ ! "$(cat ${EXCEPTION_LOG} | wc -l)" == "0" ]
  then
    echo -e "${BRed}Exception raised during tests${Color_Off}"
    cat ${EXCEPTION_LOG}
    FAIL
  fi
fi

echo -e "${BGreen}OK${Color_Off}"

echo ""
