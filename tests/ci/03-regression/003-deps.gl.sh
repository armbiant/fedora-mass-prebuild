echo -e "${BBlue}##### Check reverse dependency calculation #####${Color_Off}"

DATE=$(date)
echo -e "${Purple}Starting date: ${DATE}${Color_Off}"

echo -e "${Blue}Retrieve redhat-rpm-config reverse dependencies${Color_Off}"

mpb-whatrequires --verbose --output ${LOG_FILE} redhat-rpm-config

if [ $(cat ${LOG_FILE} | wc -l) -eq 0 ]
then
  FAIL
fi

echo -e "${Blue}Look for rpm in reverse dependencies${Color_Off}"

if ! grep -qE '^rpm' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check for support of pkgconfig(foo) in reverse dependencies${Color_Off}"

mpb-whatrequires --verbose --output ${LOG_FILE} "pkgconfig(libffi)"

if ! grep -qE 'gobject-introspection:' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check that we don't have devel packages in reverse dependencies${Color_Off}"

if grep -qE '^[a-zA-Z0-9_-]*-devel:' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check for support of rubygem(foo) in reverse dependencies${Color_Off}"

mpb-whatrequires --verbose --output ${LOG_FILE} "rubygem(pry)"

if ! grep -qE 'rubygem-pry-byebug:' ${LOG_FILE}
then
  FAIL
fi

echo -e "${Blue}Check for support of 'Core' group in reverse dependencies${Color_Off}"

mpb-whatrequires --verbose 'Core' --no-deps > ${LOG_FILE}

if ! grep -qE '^coreutils:' ${LOG_FILE}
then
  FAIL
fi

if ! grep -qE '^initscripts:' ${LOG_FILE}
then
  FAIL
fi

rm -f ${LOG_FILE}

echo -e "${BGreen}OK${Color_Off}"

echo ""
