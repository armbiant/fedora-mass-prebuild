#!/bin/bash

set -e

name=$(head -c 256 /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)

rm -f /tmp/ci-${name}.sh

for exc in "$(dirname $0)"/excerpts/*.sh "$(dirname $0)"/excerpts/before/*.sh
do
  cat ${exc} >> /tmp/ci-${name}.sh
  echo "" >> /tmp/ci-${name}.sh
done

cat "$(dirname $0)"/03-regression/*.gl.sh >> /tmp/ci-${name}.sh

cd "$(dirname $0)"
cd "$(git rev-parse --show-toplevel)"

bash /tmp/ci-${name}.sh
