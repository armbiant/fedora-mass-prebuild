# Faking Gitlab CI if we aren't already in it
if git rev-parse --is-inside-work-tree > /dev/null 2>&1;
then
  if [ "x${CI_MERGE_REQUEST_DIFF_BASE_SHA}" == "x" ]
  then
    echo -e "${Green}Fake Gitlab merge request base SHA${Color_Off}"
    export CI_MERGE_REQUEST_DIFF_BASE_SHA=$(git rev-parse origin/main)
  fi

  if [ "x${CI_PIPELINE_SOURCE}" == "x" ]
  then
    CURRENT_SHA=$(git rev-parse HEAD)

    if [ ! "x${CI_MERGE_REQUEST_DIFF_BASE_SHA}" == "x${CURRENT_SHA}" ]
    then
      echo -e "${Green}Fake Gitlab merge request event${Color_Off}"
      export CI_PIPELINE_SOURCE='merge_request_event'
    fi
  fi
fi
