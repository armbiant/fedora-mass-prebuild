if [ ! $(id -u) -eq 0 ]
then
  echo "Execute command with sudo"
  SUDO=sudo
fi

${SUDO} dnf install -y \
  npm \
  python3-dnf \
  python3-koji \
  python3-pip \
  python3-filelock \
  git \
  ruby \
  patch \


python3 --version

pip install virtualenv

virtualenv venv --system-site-packages

source venv/bin/activate
