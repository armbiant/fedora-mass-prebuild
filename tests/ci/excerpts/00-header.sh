#!/bin/bash

set -e

export TERM=xterm-256color
export PATH=${PATH}:${HOME}/.local/bin:${HOME}/bin
export PYTHONUNBUFFERED=TRUE
