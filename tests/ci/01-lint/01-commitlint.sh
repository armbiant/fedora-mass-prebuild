if [ "x${CI_PIPELINE_SOURCE}" == 'xmerge_request_event' ]
then
  echo -e "${BBlue}##### Linting commit messages #####${Color_Off}"

  npm install @commitlint/config-conventional
  npx commitlint -x @commitlint/config-conventional -f ${CI_MERGE_REQUEST_DIFF_BASE_SHA} --config=.commitlint.config.js -V
else
  echo -e "${BBlue}Skipping commit check for pipeline source ${CI_PIPELINE_SOURCE}${Color_Off}"
fi

echo ""
